//
//  PostCell.swift
//  Babylon
//
//  Created by Gabriel Massana on 27/10/2016.
//  Copyright © 2016 Gabriel Massana. All rights reserved.
//

import UIKit

import PureLayout

class PostCell: UITableViewCell, TableViewCell {

    //MARK: - Accessors
    
    /// The post related to the cell.
    var post: Post?
    
    lazy var postTitleLabel: UILabel = {
        
        var postTitleLabel = UILabel.newAutoLayout()
        
        postTitleLabel.font = UIFont.tradeGothicLTWithSize(18.0)
        postTitleLabel.textAlignment = .left
        postTitleLabel.textColor = UIColor.black
        postTitleLabel.numberOfLines = 0
        
        return postTitleLabel
    }()
    
    lazy var separationLine: UIView = {
        
        var separationLine = UIView.newAutoLayout()
        
        separationLine.backgroundColor = UIColor.neptune()
        
        return separationLine
    }()
    
    //MARK: - Init
    
    override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
        
        super.init(style: style,
                   reuseIdentifier: reuseIdentifier)
        
        selectionStyle = .gray
        
        contentView.addSubview(separationLine)
        contentView.addSubview(postTitleLabel)
    }
    
    required init?(coder aDecoder: NSCoder) {
        
        super.init(coder: aDecoder)
    }
    
    //MARK: - Constraints
    
    override func updateConstraints() {
        
        postTitleLabel.autoPinEdge(toSuperviewEdge: .left,
                                   withInset: 10.0 * DeviceManager.sharedInstance.resizeFactor)
        
        postTitleLabel.autoPinEdge(toSuperviewEdge: .right,
                                   withInset: 10.0 * DeviceManager.sharedInstance.resizeFactor)
        
        postTitleLabel.autoPinEdge(toSuperviewEdge: .top,
                                   withInset: 10.0 * DeviceManager.sharedInstance.resizeFactor)
        
        NSLayoutConstraint.autoSetPriority(UILayoutPriorityDefaultHigh) {
            
            postTitleLabel.autoPinEdge(toSuperviewEdge: .bottom,
                                       withInset: 10.0 * DeviceManager.sharedInstance.resizeFactor)
        }
        
        /*-------------------*/
        
        separationLine.autoPinEdge(toSuperviewEdge: .bottom)
        
        separationLine.autoSetDimension(.height,
                                        toSize: 0.5 * DeviceManager.sharedInstance.resizeFactor)
        
        separationLine.autoPinEdge(toSuperviewEdge: .right)
        
        separationLine.autoPinEdge(toSuperviewEdge: .left,
                                   withInset: 10.0 * DeviceManager.sharedInstance.resizeFactor)
        
        /*-------------------*/
        
        super.updateConstraints()
    }
    
    //MARK: - ConfigureCell
    
    /**
     Configure the self cell based on the post data.
     
     - Parameter post: The post with the data related to the cell.
     */
    func configureWithPost(_ post: Post) {
        
        self.post = post
        
        postTitleLabel.text = post.title
    }
    
    //MARK: - PrepareForReuse
    
    override func prepareForReuse() {
        
        super.prepareForReuse()
        
    }
}
