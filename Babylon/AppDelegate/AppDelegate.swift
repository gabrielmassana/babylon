//
//  AppDelegate.swift
//  Babylon
//
//  Created by Gabriel Massana on 26/10/2016.
//  Copyright © 2016 Gabriel Massana. All rights reserved.
//

import UIKit

import CoreDataFullStack

/// Operation Queue identifier for Local Data Operations.
let LocalDataOperationQueueTypeIdentifier: String = "LocalDataOperationQueueTypeIdentifier"

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    //MARK: - Accessors
    
    /// App Window
    var window: UIWindow? = {
        
        let window: UIWindow = UIWindow(frame: UIScreen.main.bounds)
        
        window.backgroundColor = UIColor.white
        
        return window
    }()

    /// The Navigation Controller working as rootViewController.
    lazy var rootNavigationController: RootNavigationController = {
        
        var rootNavigationController = RootNavigationController()
        
        return rootNavigationController
    }()

    //MARK: - UIApplicationDelegate

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {

        CDFCoreDataManager.sharedInstance().delegate = self
        
        /*-------------------*/

        BuddyBuildSDK.setup()
        
        /*-------------------*/

        registerOperationQueues()
        
        /*-------------------*/
        
        window!.rootViewController = rootNavigationController
        window!.makeKeyAndVisible()
        
        UINavigationBar.appearance().barTintColor = UIColor.neptune()
        UITableViewHeaderFooterView.appearance().contentView.tintColor = UIColor.alto()
        
        UILabel.appearance(whenContainedInInstancesOf: [UITableViewHeaderFooterView.self]).textColor = UIColor.neptune()
        UILabel.appearance(whenContainedInInstancesOf: [UITableViewHeaderFooterView.self]).font = UIFont.tradeGothicNo2BoldWithSize(20.0)
        UIView.appearance(whenContainedInInstancesOf: [UITableViewHeaderFooterView.self]).backgroundColor = UIColor.alto()
        
        return true
    }
    
    //MARK: - OperationQueues
    
    /**
     Registers the operations queues in the app.
     */
    func registerOperationQueues() {
        
        // e.g.: Parse and Core Data updates
        let localDataOperationQueue: OperationQueue = OperationQueue()
        
        localDataOperationQueue.qualityOfService = .userInteractive
        localDataOperationQueue.maxConcurrentOperationCount = OperationQueue.defaultMaxConcurrentOperationCount
        OperationQueueManager.sharedInstance.register(operationQueue: localDataOperationQueue,
                                                      operationQueueIdentifier: LocalDataOperationQueueTypeIdentifier)
    }
}

extension AppDelegate : CDFCoreDataManagerDelegate {
    
    public func coreDataModelName() -> String! {
        
        return "Babylon"
    }
}
