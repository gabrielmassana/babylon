//
//  UsersParseOperation.swift
//  Babylon
//
//  Created by Gabriel Massana on 26/10/2016.
//  Copyright © 2016 Gabriel Massana. All rights reserved.
//

import UIKit

import CoreDataFullStack

class UsersParseOperation: Operation {

    //MARK: - Accessors
    
    /// The response from API to be parsed.
    var responseObject: [[String : AnyObject]]
    
    //MARK: - Init
    
    init(responseObject: [[String : AnyObject]]) {
        
        self.responseObject = responseObject
        
        super.init()
        
        identifier = "UsersParseOperation - \(NSDate())"
    }
    
    //MARK: - Start
    
    override func start() {
        
        super.start()
        
        guard let backgroundManagedObjectContext = CDFCoreDataManager.sharedInstance().backgroundManagedObjectContext else {
            
            didCompleteWithResult(nil)
            return
        }
        
        backgroundManagedObjectContext.performAndWait {
            
            //Parse
            let usersParser = UsersParser.parser(backgroundManagedObjectContext)
            
            let users = usersParser.parseUsers(self.responseObject)
            
            CDFCoreDataManager.sharedInstance().saveManagedObjectContext(backgroundManagedObjectContext)
            
            self.didCompleteWithResult(users as AnyObject?)
        }
    }
    
    //MARK: - Cancel
    
    override func cancel() {
        
        super.cancel()
        
        didCompleteWithResult(nil)
    }
}
