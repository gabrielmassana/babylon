//
//  PostsAPIManager.swift
//  Babylon
//
//  Created by Gabriel Massana on 26/10/2016.
//  Copyright © 2016 Gabriel Massana. All rights reserved.
//

import UIKit

class PostsAPIManager: APIManager {
    
    //MARK: - Posts
    
    class func retrievePosts(success: @escaping NetworkingOnSuccess, failure: @escaping NetworkingOnFailure) {
        
        let request = Request.posts()
        
        NetworkingManager.defaultManager.request(request)
            .validate()
            .responseJSONBabylon(completionHandler: { (response, error) in
                
                switch response.result {
                    
                case .success:
                    
                    if let responseObject = response.result.value as? [[String : AnyObject]] {
                        
                        //Parse here
                        let operation = PostsParseOperation(responseObject: responseObject)
                        
                        operation.operationQueueIdentifier = LocalDataOperationQueueTypeIdentifier
                        
                        operation.onCompletion = { (result: AnyObject?) -> Void in
                            
                            success(result)
                        }
                        
                        OperationQueueManager.sharedInstance.add(operation: operation)
                    }
                    else {
                        
                        failure(error)
                    }
                    
                case .failure(_):
                    
                    failure(error)
                }
            })
    }
}
