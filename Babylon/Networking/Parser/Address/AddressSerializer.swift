//
//  AddressSerializer.swift
//  Babylon
//
//  Created by Gabriel Massana on 27/10/2016.
//  Copyright © 2016 Gabriel Massana. All rights reserved.
//

import Foundation

struct AddressSerializer {
    
    var city: String?
    var latitude: NSNumber?
    var longitude: NSNumber?
    var street: String?
    var suite: String?
    var zipcode: String?
    
    init?(serverResponse: [String : AnyObject]) {

        if let city = serverResponse["city"] as? String {
            
            self.city = city
        }
        
        if let suite = serverResponse["suite"] as? String {
            
            self.suite = suite
        }
        
        if let street = serverResponse["street"] as? String {
            
            self.street = street
        }
        
        if let zipcode = serverResponse["zipcode"] as? String {
            
            self.zipcode = zipcode
        }
        
        if let geo = serverResponse["geo"] as? [String : AnyObject],
            let lat = geo["lat"] as? String,
            let latitude = Float(lat) {
            
            self.latitude = latitude as NSNumber?
        }
        
        if let geo = serverResponse["geo"] as? [String : AnyObject],
            let lng = geo["lng"] as? String,
            let longitude = Float(lng) {
            
            self.longitude = longitude as NSNumber?
        }
    }
}
