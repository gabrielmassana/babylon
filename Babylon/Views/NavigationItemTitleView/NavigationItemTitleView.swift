//
//  NavigationItemTitleView.swift
//  Babylon
//
//  Created by Gabriel Massana on 27/10/2016.
//  Copyright © 2016 Gabriel Massana. All rights reserved.
//

import UIKit

class NavigationItemTitleView: UILabel {
    
    //MARK: - Init
    
    init(labelText: String) {
        
        let labelFont = UIFont.tradeGothicNo2BoldWithSize(24.0)
        
        let boundingRectSize = CGSize(width: 220.0 * DeviceManager.sharedInstance.resizeFactor,
                                      height: 40.0 * DeviceManager.sharedInstance.resizeFactor)
        
        let titleTextSize = labelText.sizeForText(labelFont,
                                                  boundingRectSize: boundingRectSize)
        
        let frame = CGRect(x: 0.0,
                           y: 0.0,
                           width: titleTextSize.width,
                           height: titleTextSize.height)
        
        super.init(frame: frame)
        
        textAlignment = .center
        textColor = UIColor.white
        font = labelFont
        lineBreakMode = NSLineBreakMode.byTruncatingMiddle
        text = labelText
    }
    
    required init?(coder aDecoder: NSCoder) {
        
        fatalError("init(coder:) has not been implemented")
    }
}
