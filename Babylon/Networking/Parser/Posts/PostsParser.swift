//
//  PostsParser.swift
//  Babylon
//
//  Created by Gabriel Massana on 26/10/2016.
//  Copyright © 2016 Gabriel Massana. All rights reserved.
//

import UIKit

import CoreDataFullStack

class PostsParser: Parser {

    func parsePosts(_ serverResponse: [[String : AnyObject]]) -> [Post] {
        
        var posts = [Post]()
        
        for serverPost in serverResponse {
            
            let post = parsePost(serverPost)
            
            if let post = post {
                
                posts.append(post)
            }
        }
        
        return posts
    }
    
    fileprivate func parsePost(_ serverResponse: [String : AnyObject]) -> Post? {
        
        // Serializer
        guard let postSerializer = PostSerializer(serverResponse: serverResponse),
            let parserManagedObjectContext = parserManagedObjectContext else {
            
            return nil
        }
        
        // Fetch Post
        var post = Post.fetchPost(postSerializer.postID,
                                  managedObjectContext: parserManagedObjectContext)
        
        if post == nil {
            
            post = CDFInsertService.insertNewObject(forEntityClass: Post.self,
                                                    in: parserManagedObjectContext) as? Post
            
            post?.postID = postSerializer.postID
        }

        post?.body = postSerializer.body
        post?.title = postSerializer.title
        
        // Fetch User
        var user = User.fetchUser(postSerializer.userID,
                                  managedObjectContext: parserManagedObjectContext)
        
        if user == nil {
            
            user = CDFInsertService.insertNewObject(forEntityClass: User.self,
                                                    in: parserManagedObjectContext) as? User
            
            user?.userID = postSerializer.userID
        }
        
        post?.user = user

        return post
    }
}
