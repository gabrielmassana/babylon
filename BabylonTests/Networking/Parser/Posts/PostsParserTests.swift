//
//  PostsParserTests.swift
//  Babylon
//
//  Created by Gabriel Massana on 30/10/2016.
//  Copyright © 2016 Gabriel Massana. All rights reserved.
//

import XCTest

import CoreDataFullStack

@testable import Babylon

class PostsParserTests: XCTestCase {
    
    //MARK: - Accessors
    
    var parser: PostsParser?
    
    var postsJSON: [[String : AnyObject]]?
    var postJSON: [String : AnyObject]?
    
    var userID: NSNumber?
    var id: NSNumber?
    var title: String?
    var body: String?
    
    // MARK: - TestSuiteLifecycle
    
    override func setUp() {
        
        super.setUp()
        
        parser = PostsParser.parser(CDFCoreDataManager.sharedInstance().managedObjectContext)
        
        userID = 1
        id = 1
        title = "sunt aut facere repellat provident occaecati excepturi optio reprehenderit"
        body = "quia et suscipit\nsuscipit recusandae consequuntur expedita et cum\nreprehenderit molestiae ut ut quas totam\nnostrum rerum est autem sunt rem eveniet architecto"
        
        postJSON = [
            "userId": userID!,
            "id": id!,
            "title": title! as AnyObject,
            "body": body! as AnyObject
        ]
        
        postsJSON = [
            postJSON!,
            [
                "userId": 1 as NSNumber,
                "id": 2 as NSNumber,
                "title": "qui est esse" as AnyObject,
                "body": "est rerum tempore vitae\nsequi sint nihil reprehenderit dolor beatae ea dolores neque\nfugiat blanditiis voluptate porro vel nihil molestiae ut reiciendis\nqui aperiam non debitis possimus qui neque nisi nulla" as AnyObject
            ],
            [
                "userId": 1 as NSNumber,
                "id": 3 as NSNumber,
                "title": "ea molestias quasi exercitationem repellat qui ipsa sit aut" as AnyObject,
                "body": "et iusto sed quo iure\nvoluptatem occaecati omnis eligendi aut ad\nvoluptatem doloribus vel accusantium quis pariatur\nmolestiae porro eius odio et labore et velit aut" as AnyObject
            ]
        ]
    }
    
    override func tearDown() {
        
        CDFCoreDataManager.sharedInstance().reset()
        
        parser = nil
        
        postsJSON = nil
        postJSON = nil

        userID = nil
        id = nil
        title = nil
        body = nil
        
        super.tearDown()
    }
    
    // MARK: - PostsParser

    func test_postsParser_newPostObjectReturned() {
        
        let posts = parser?.parsePosts(postsJSON!)
        
        XCTAssertNotNil(posts, "A valid Posts Array object wasn't created")
    }
    
    func test_postsParser_count() {
        
        let posts = parser?.parsePosts(postsJSON!)
        
        let arrayCount = NSNumber(value: posts!.count)
        let jsonCount = NSNumber(value: postsJSON!.count as Int)
        
        XCTAssertTrue(arrayCount == jsonCount, String(format:"Parsed count is wrong. Should be %@ rather than: %@", arrayCount, jsonCount))
    }
    
    func test_parsePosts_uniqueObjects() {
        
        let posts = parser?.parsePosts(postsJSON!)
        
        let firstObject = posts!.first!
        let lastObject = posts!.last!
        
        XCTAssertNotEqual(firstObject, lastObject, String(format:"Parsed objects are wrong: %@ and %@", firstObject, lastObject))
    }
    
    func test_parsePosts_firstObject_postID() {
        
        let posts = parser?.parsePosts(postsJSON!)
        
        let firstObject = posts!.first!
        
        XCTAssertEqual(firstObject.postID, id!.stringValue, String(format:"Parsed object is wrong: %@", firstObject))
    }
    
    func test_parsePosts_firstObject_userID() {
        
        let posts = parser?.parsePosts(postsJSON!)
        
        let firstObject = posts!.first!
        
        XCTAssertEqual(firstObject.user!.userID, userID!.stringValue, String(format:"Parsed object is wrong: %@", firstObject))
    }
    
    func test_parsePosts_firstObject_title() {
        
        let posts = parser?.parsePosts(postsJSON!)
        
        let firstObject = posts!.first!
        
        XCTAssertEqual(firstObject.title, title!, String(format:"Parsed object is wrong: %@", firstObject))
    }
    
    func test_parsePosts_firstObject_body() {
        
        let posts = parser?.parsePosts(postsJSON!)
        
        let firstObject = posts!.first!
        
        XCTAssertEqual(firstObject.body, body!, String(format:"Parsed object is wrong: %@", firstObject))
    }
}
