//
//  PostSerializer.swift
//  Babylon
//
//  Created by Gabriel Massana on 26/10/2016.
//  Copyright © 2016 Gabriel Massana. All rights reserved.
//

import Foundation

struct PostSerializer {
    
    var postID: String
    var userID: String

    var body: String?
    var title: String?
    
    init?(serverResponse: [String : AnyObject]) {
        
        guard let postID = serverResponse["id"] as? NSNumber,
            let userID = serverResponse["userId"] as? NSNumber else {
                
                return nil
        }
        
        self.postID = postID.stringValue
        self.userID = userID.stringValue
        
        if let body = serverResponse["body"] as? String {
            
            self.body = body
        }
        
        if let title = serverResponse["title"] as? String {
            
            self.title = title
        }

    }
}
