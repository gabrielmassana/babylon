//
//  DetailsViewController.swift
//  Babylon
//
//  Created by Gabriel Massana on 28/10/2016.
//  Copyright © 2016 Gabriel Massana. All rights reserved.
//

import UIKit

class DetailsViewController: UIViewController {

    //MARK: - Accessors
    
    var post: Post
    
    /// Table view to display the posts.
    lazy var tableView: UITableView = {
        
        let tableView: UITableView = UITableView(frame: CGRect.zero,
                                                 style: .grouped)
        
        //tableView.translatesAutoresizingMaskIntoConstraints = false

        return tableView
    }()
    
    /// Adapter to manage the common logic and data of the tableView.
    lazy var adapter: DetailsAdapter = {
        
        let adapter = DetailsAdapter(post: self.post)
        
        return adapter
    }()
    
    lazy var navigationItemTitleView: NavigationItemTitleView = {
        
        let navigationItemTitleView = NavigationItemTitleView(labelText: NSLocalizedString("Post Details", comment: "post_details_title"))
        
        return navigationItemTitleView
    }()
    
    lazy var backButton: UIButton = {
        
        var backButton = UIButton(type: .custom)
        
        backButton.frame = CGRect(x: 0.0,
                              y: 0.0,
                              width: 45.0,
                              height: 30.0)
        
        backButton.addTarget(self,
                         action: #selector(backButtonPressed(_:)),
                         for: .touchUpInside)
        
        backButton.setTitleColor(UIColor.white,
                             for: .normal)
        
        backButton.titleLabel?.font = UIFont.tradeGothicLTWithSize(17.0)
        backButton.titleLabel?.textAlignment = .left
        
        backButton.setTitle(NSLocalizedString("Back", comment: "back_title"),
                        for: .normal)
        
        return backButton
    }()
    
    lazy var backBarButtonItem: UIBarButtonItem = {
        
        var backBarButtonItem = UIBarButtonItem(customView: self.backButton)
        
        return backBarButtonItem
    }()
    
    //MARK: - Init

    init(post: Post) {
       
        self.post = post
        
        super.init(nibName: nil,
                   bundle: nil)
    }
    
    required init?(coder aDecoder: NSCoder) {
        
        fatalError("init(coder:) has not been implemented")
    }
    
    //MARK: - ViewLifeCycle

    override func viewDidLoad() {
        
        super.viewDidLoad()

        view.addSubview(tableView)
        adapter.tableView = tableView
        
        /*-------------------*/
        
        updateViewConstraints()

        /*-------------------*/
        
        navigationItem.titleView = navigationItemTitleView
        navigationItem.leftBarButtonItem = backBarButtonItem
    }
    
    //MARK: - Constraints
    
    override func updateViewConstraints() {
        
        super.updateViewConstraints()
        
        /*-------------------*/
        
        tableView.autoPinEdgesToSuperviewEdges()
    }
    
    //MARK: - ButtonActions
    
    func backButtonPressed(_ sender: UIButton) {
        
        _ = self.navigationController?.popViewController(animated: true)
    }
}
