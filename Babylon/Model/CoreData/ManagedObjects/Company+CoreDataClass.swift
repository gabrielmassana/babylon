//
//  Company+CoreDataClass.swift
//
//
//  Created by Gabriel Massana on 26/10/2016.
//
//

import Foundation
import CoreData

import CoreDataFullStack

@objc(Company)
public class Company: NSManagedObject {
    
    class func fetchCompany(_ userID: String, managedObjectContext: NSManagedObjectContext) -> Company? {
        
        let predicate = NSPredicate(format: "user.userID == %@", userID)
        
        let company = CDFRetrievalService.retrieveFirstEntry(forEntityClass: Company.self,
                                                             predicate: predicate,
                                                             managedObjectContext: managedObjectContext) as? Company
        
        return company
    }
}
