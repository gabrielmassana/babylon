//
//  CommentsParser.swift
//  Babylon
//
//  Created by Gabriel Massana on 26/10/2016.
//  Copyright © 2016 Gabriel Massana. All rights reserved.
//

import UIKit

import CoreDataFullStack

class CommentsParser: Parser {

    func parseComments(_ serverResponse: [[String : AnyObject]]) -> [Comment] {
        
        var comments = [Comment]()

        for serverComment in serverResponse {
            
            let comment = parseComment(serverComment)
            
            if let comment = comment {
                
                comments.append(comment)
            }
        }
        
        return comments
    }
    
    fileprivate func parseComment(_ serverResponse: [String : AnyObject]) -> Comment? {

        // Serializer
        guard let commentSerializer = CommentSerializer(serverResponse: serverResponse),
            let parserManagedObjectContext = parserManagedObjectContext else {
                
                return nil
        }
        
        // Fetch Post
        var comment = Comment.fetchComment(commentSerializer.commentID,
                                           managedObjectContext: parserManagedObjectContext)
        
        if comment == nil {
            
            comment = CDFInsertService.insertNewObject(forEntityClass: Comment.self,
                                                       in: parserManagedObjectContext) as? Comment
            
            comment?.commentID = commentSerializer.commentID
        }
        
        comment?.body = commentSerializer.body
        comment?.email = commentSerializer.email
        comment?.name = commentSerializer.name
        
        // Fetch Post
        var post = Post.fetchPost(commentSerializer.postID,
                                  managedObjectContext: parserManagedObjectContext)

        if post == nil {
            
            post = CDFInsertService.insertNewObject(forEntityClass: Post.self,
                                                    in: parserManagedObjectContext) as? Post
            
            post?.postID = commentSerializer.postID
        }

        comment?.post = post
        
        return comment
    }
}
