//
//  UsersParserTests.swift
//  Babylon
//
//  Created by Gabriel Massana on 30/10/2016.
//  Copyright © 2016 Gabriel Massana. All rights reserved.
//

import XCTest

import CoreDataFullStack

@testable import Babylon

class UsersParserTests: XCTestCase {
    
    //MARK: - Accessors
    
    var parser: UsersParser?
    
    var usersJSON: [[String : AnyObject]]?
    var userJSON: [String : AnyObject]?
    
    var id: NSNumber?
    var userName: String?
    var username: String?
    var email: String?
    var address: [String : AnyObject]?
    var phone: String?
    var website: String?
    var company: [String : AnyObject]?

    // MARK: - TestSuiteLifecycle
    
    override func setUp() {
        
        super.setUp()
        
        parser = UsersParser.parser(CDFCoreDataManager.sharedInstance().managedObjectContext)
        
        id = 1
        userName = "Leanne Graham"
        username = "Bret"
        email = "Sincere@april.biz"
        phone = "1-770-736-8031 x56442"
        website = "hildegard.org"

        address = [
            "street" : "Kulas Light" as AnyObject,
            "suite" : "Apt. 556" as AnyObject,
            "city" : "Gwenborough" as AnyObject,
            "zipcode" : "92998-3874" as AnyObject,
            "geo" : [
                "lat" : "-37.3159",
                "lng" : "81.1496"
            ] as AnyObject
        ]
        
        company = [
            "name" : "Romaguera-Crona" as AnyObject,
            "catchPhrase" : "Multi-layered client-server neural-net" as AnyObject,
            "bs" : "harness real-time e-markets" as AnyObject
        ]
        
        userJSON = [
            "id": id!,
            "name": userName! as AnyObject,
            "username": username! as AnyObject,
            "email": email! as AnyObject,
            "address": address! as AnyObject,
            "phone": phone! as AnyObject,
            "website": website! as AnyObject,
            "company": company! as AnyObject
        ]
        
        usersJSON = [
            userJSON!,
            [
                "id": 2 as NSNumber,
                "name": "Ervin Howell" as AnyObject,
                "username": "Antonette" as AnyObject,
                "email": "Shanna@melissa.tv" as AnyObject,
                "address": [
                    "street": "Victor Plains",
                    "suite": "Suite 879",
                    "city": "Wisokyburgh",
                    "zipcode": "90566-7771",
                    "geo": [
                        "lat": "-43.9509",
                        "lng": "-34.4618"
                    ] as AnyObject
                ] as AnyObject,
                "phone": "010-692-6593 x09125" as AnyObject,
                "website": "anastasia.net" as AnyObject,
                "company": [
                    "name": "Deckow-Crist",
                    "catchPhrase": "Proactive didactic contingency",
                    "bs": "synergize scalable supply-chains"
                ] as AnyObject
            ],
            [
                "id": 3 as NSNumber,
                "name": "Clementine Bauch" as AnyObject,
                "username": "Samantha" as AnyObject,
                "email": "Nathan@yesenia.net" as AnyObject,
                "address": [
                    "street": "Douglas Extension",
                    "suite": "Suite 847",
                    "city": "McKenziehaven",
                    "zipcode": "59590-4157",
                    "geo": [
                        "lat": "-68.6102",
                        "lng": "-47.0653"
                    ] as AnyObject
                ] as AnyObject,
                "phone": "1-463-123-4447" as AnyObject,
                "website": "ramiro.info" as AnyObject,
                "company": [
                    "name": "Romaguera-Jacobson",
                    "catchPhrase": "Face to face bifurcated interface",
                    "bs": "e-enable strategic applications"
                ] as AnyObject
            ]
        ]
    }
    
    override func tearDown() {
        
        CDFCoreDataManager.sharedInstance().reset()
        
        parser = nil
        
        id = nil
        userName = nil
        username = nil
        email = nil
        phone = nil
        website = nil
        address = nil
        company = nil
        
        userJSON = nil
        usersJSON = nil
        
        super.tearDown()
    }
    
    // MARK: - UsersParser
    
    func test_usersParser_newUserObjectReturned() {
        
        let users = parser?.parseUsers(usersJSON!)
        
        XCTAssertNotNil(users, "A valid Users Array object wasn't created")
    }
    
    func test_usersParser_count() {
        
        let users = parser?.parseUsers(usersJSON!)
        
        let arrayCount = NSNumber(value: users!.count)
        let jsonCount = NSNumber(value: usersJSON!.count as Int)
        
        XCTAssertTrue(arrayCount == jsonCount, String(format:"Parsed count is wrong. Should be %@ rather than: %@", arrayCount, jsonCount))
    }
    
    func test_parseUsers_uniqueObjects() {
        
        let users = parser?.parseUsers(usersJSON!)
        
        let firstObject = users!.first!
        let lastObject = users!.last!
        
        XCTAssertNotEqual(firstObject, lastObject, String(format:"Parsed objects are wrong: %@ and %@", firstObject, lastObject))
    }
    
    func test_parseUsers_company() {
        
        let users = parser?.parseUsers(usersJSON!)
        
        let firstObject = users!.first!
        
        XCTAssertNotNil(firstObject.address!, "A valid Company object wasn't created")
    }
    
    func test_parseUsers_address() {
        
        let users = parser?.parseUsers(usersJSON!)
        
        let firstObject = users!.first!
        
        XCTAssertNotNil(firstObject.company!, "A valid Address object wasn't created")
    }
    
    func test_parseUsers_firstObject_userID() {
        
        let users = parser?.parseUsers(usersJSON!)
        
        let firstObject = users!.first!
        
        XCTAssertEqual(firstObject.userID, id!.stringValue, String(format:"Parsed object is wrong: %@", firstObject))
    }
    
    func test_parseUsers_firstObject_name() {
        
        let users = parser?.parseUsers(usersJSON!)
        
        let firstObject = users!.first!
        
        XCTAssertEqual(firstObject.name, userName!, String(format:"Parsed object is wrong: %@", firstObject))
    }
    
    func test_parseUsers_firstObject_username() {
        
        let users = parser?.parseUsers(usersJSON!)
        
        let firstObject = users!.first!
        
        XCTAssertEqual(firstObject.username, username!, String(format:"Parsed object is wrong: %@", firstObject))
    }
    
    func test_parseUsers_firstObject_email() {
        
        let users = parser?.parseUsers(usersJSON!)
        
        let firstObject = users!.first!
        
        XCTAssertEqual(firstObject.email, email!, String(format:"Parsed object is wrong: %@", firstObject))
    }
    
    func test_parseUsers_firstObject_phone() {
        
        let users = parser?.parseUsers(usersJSON!)
        
        let firstObject = users!.first!
        
        XCTAssertEqual(firstObject.phone, phone!, String(format:"Parsed object is wrong: %@", firstObject))
    }
    
    func test_parseUsers_firstObject_website() {
        
        let users = parser?.parseUsers(usersJSON!)
        
        let firstObject = users!.first!
        
        XCTAssertEqual(firstObject.website, website!, String(format:"Parsed object is wrong: %@", firstObject))
    }
}
