//
//  UserAddressCell.swift
//  Babylon
//
//  Created by Gabriel Massana on 29/10/2016.
//  Copyright © 2016 Gabriel Massana. All rights reserved.
//

import UIKit

class UserAddressCell: UITableViewCell, TableViewCell {
    
    //MARK: - Accessors
    
    var post: Post
    
    lazy var userAddressLabel: UILabel = {
        
        var userAddressLabel = UILabel.newAutoLayout()
        
        userAddressLabel.font = UIFont.tradeGothicLTWithSize(16.0)
        userAddressLabel.textAlignment = .left
        userAddressLabel.textColor = UIColor.black
        userAddressLabel.numberOfLines = 0
        
        return userAddressLabel
    }()
    
    //MARK: - Init
    
    init(post: Post) {
        
        self.post = post
        
        super.init(style: .default,
                   reuseIdentifier: nil)
        
        selectionStyle = .none
        
        contentView.addSubview(userAddressLabel)
        
        configureWithPost(post)
    }
    
    required init?(coder aDecoder: NSCoder) {
        
        fatalError("init(coder:) has not been implemented")
    }
    
    //MARK: - Constraints
    
    override func updateConstraints() {
        
        userAddressLabel.autoPinEdge(toSuperviewEdge: .left,
                                     withInset: 15.0 * DeviceManager.sharedInstance.resizeFactor)
        
        userAddressLabel.autoPinEdge(toSuperviewEdge: .right,
                                     withInset: 15.0 * DeviceManager.sharedInstance.resizeFactor)
        
        userAddressLabel.autoPinEdge(toSuperviewEdge: .top,
                                     withInset: 10.0 * DeviceManager.sharedInstance.resizeFactor)
        
        NSLayoutConstraint.autoSetPriority(UILayoutPriorityDefaultHigh) {
            
            userAddressLabel.autoPinEdge(toSuperviewEdge: .bottom,
                                         withInset: 10.0 * DeviceManager.sharedInstance.resizeFactor)
        }
        
        /*-------------------*/
        
        super.updateConstraints()
    }
    
    //MARK: - ConfigureCell
    
    /**
     Configure the self cell based on the post data.
     
     - Parameter post: The post with the data related to the cell.
     */
    private func configureWithPost(_ post: Post) {
        
        self.post = post
        
        guard let user = post.user,
            let address = user.address,
            let city = address.city,
            let street = address.street,
            let zipcode = address.zipcode else {
                
                return
        }
        
        let cityTitle = "\(NSLocalizedString("City: ", comment: "city_title"))\(city)\n"
        let streetTitle = "\(NSLocalizedString("Street: ", comment: "street_title"))\(street)\n"
        let zipcodeTitle = "\(NSLocalizedString("zipcode: ", comment: "Zipcode_title"))\(zipcode)\n"
        
        userAddressLabel.text = cityTitle + streetTitle + zipcodeTitle
    }
}
