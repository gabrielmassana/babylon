//
//  UIColor+Babylon.swift
//  Babylon
//
//  Created by Gabriel Massana on 27/10/2016.
//  Copyright © 2016 Gabriel Massana. All rights reserved.
//
// Naming: http://chir.ag/projects/name-that-color/
//

import UIKit

import ColorWithHex

// Accessors for all the colors used.
extension UIColor {
    
    class func scorpion() -> UIColor {
        
        return UIColor.colorWithHex("606060")!
    }
    
    class func alto() -> UIColor {
        
        return UIColor.colorWithHex("DFDFDF")!
    }
    
    class func neptune() -> UIColor {
        
        return UIColor.colorWithHex("72BEBE")!
    }
}
