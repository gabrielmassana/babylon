//
//  PostsParseOperation.swift
//  Babylon
//
//  Created by Gabriel Massana on 26/10/2016.
//  Copyright © 2016 Gabriel Massana. All rights reserved.
//

import UIKit

import CoreDataFullStack

class PostsParseOperation: Operation {

    //MARK: - Accessors
    
    /// The response from API to be parsed.
    var responseObject: [[String : AnyObject]]
    
    //MARK: - Init
    
    init(responseObject: [[String : AnyObject]]) {
        
        self.responseObject = responseObject
        
        super.init()
        
        identifier = "PostsParseOperation - \(NSDate())"
    }
    
    //MARK: - Start
    
    override func start() {
        
        super.start()
        
        guard let backgroundManagedObjectContext = CDFCoreDataManager.sharedInstance().backgroundManagedObjectContext else {
            
            didCompleteWithResult(nil)
            return
        }
        
        backgroundManagedObjectContext.performAndWait {
            
            //Parse
            let postsParser = PostsParser.parser(backgroundManagedObjectContext)
            
            let posts = postsParser.parsePosts(self.responseObject)
            
            CDFCoreDataManager.sharedInstance().saveManagedObjectContext(backgroundManagedObjectContext)
            
            self.didCompleteWithResult(posts as AnyObject?)
        }
    }
    
    //MARK: - Cancel
    
    override func cancel() {
        
        super.cancel()
        
        didCompleteWithResult(nil)
    }
}
