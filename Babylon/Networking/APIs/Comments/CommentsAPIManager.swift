//
//  CommentsAPIManager.swift
//  Babylon
//
//  Created by Gabriel Massana on 26/10/2016.
//  Copyright © 2016 Gabriel Massana. All rights reserved.
//

import UIKit

class CommentsAPIManager: APIManager {

    //MARK: - Comments
    
    class func retrieveComments(success: @escaping NetworkingOnSuccess, failure: @escaping NetworkingOnFailure) {
        
        let request = Request.comments()
        
        NetworkingManager.defaultManager.request(request)
            .validate()
            .responseJSONBabylon(completionHandler: { (response, error) in
                
                switch response.result {
                    
                case .success:
                    
                    if let responseObject = response.result.value as? [[String : AnyObject]] {
                        
                        //Parse here
                        let operation = CommentsParseOperation(responseObject: responseObject)
                        
                        operation.operationQueueIdentifier = LocalDataOperationQueueTypeIdentifier
                        
                        operation.onCompletion = { (result: AnyObject?) -> Void in
                            
                            success(result)
                        }
                        
                        OperationQueueManager.sharedInstance.add(operation: operation)
                    }
                    else {
                        
                        failure(error)
                    }
                    
                case .failure(_):
                    
                    failure(error)
                }
            })
    }
}
