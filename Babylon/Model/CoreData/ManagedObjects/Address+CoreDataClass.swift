//
//  Address+CoreDataClass.swift
//
//
//  Created by Gabriel Massana on 26/10/2016.
//
//

import Foundation
import CoreData

import CoreDataFullStack

@objc(Address)
public class Address: NSManagedObject {
    
    class func fetchAddress(_ userID: String, managedObjectContext: NSManagedObjectContext) -> Address? {
        
        let predicate = NSPredicate(format: "user.userID == %@", userID)
        
        let address = CDFRetrievalService.retrieveFirstEntry(forEntityClass: Address.self,
                                                             predicate: predicate,
                                                             managedObjectContext: managedObjectContext) as? Address
        
        return address
    }
}
