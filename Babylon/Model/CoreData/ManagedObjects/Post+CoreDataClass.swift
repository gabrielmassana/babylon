//
//  Post+CoreDataClass.swift
//  
//
//  Created by Gabriel Massana on 26/10/2016.
//
//

import Foundation

import CoreData
import CoreDataFullStack

@objc(Post)
public class Post: NSManagedObject {

    class func fetchPost(_ postID: String, managedObjectContext: NSManagedObjectContext) -> Post? {
        
        let predicate = NSPredicate(format: "postID == %@", postID)
        
        let post = CDFRetrievalService.retrieveFirstEntry(forEntityClass: Post.self,
                                                          predicate: predicate,
                                                          managedObjectContext: managedObjectContext) as? Post
        
        return post
    }
}
