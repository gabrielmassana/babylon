//
//  OperationQueueManager.swift
//  Babylon
//
//  Created by Gabriel Massana on 26/10/2016.
//  Copyright © 2016 Gabriel Massana. All rights reserved.
//

import UIKit
import Foundation

class OperationQueueManager: NSObject {
    
    //MARK: - Singleton
    
    /// Returns the global OperationQueueManager instance.
    static let sharedInstance = OperationQueueManager()
    
    //MARK: - Accessors
    
    /// Dictionary with the NSOperationQueue already setted up.
    internal var operationQueuesDictionary = [String: OperationQueue]()
    
    //MARK: - Init
    
    override init() {
        
    }
    
    //MARK: - Register
    
    /**
     Registers an operation queue.
     
     - Parameter operationQueue: the new operation queue to be registered.
     - Parameter operationQueueIdentifier: the new operation queue identifier.
     */
    func register(operationQueue: OperationQueue, operationQueueIdentifier: String) {
        
        operationQueuesDictionary[operationQueueIdentifier] = operationQueue
    }
    
    //MARK: - AddOperation
    
    /**
     Adds an operation to an operation queue.
     
     - Parameter operation: the new operation to be added.
     */
    func add(operation: Operation) {
        
        guard let operationQueueIdentifier = operation.operationQueueIdentifier,
            let operationQueue = operationQueuesDictionary[operationQueueIdentifier] else {
                
                return
        }
        
        let coalescedOperation = coalesce(operation: operation, operationQueue: operationQueue)
        
        // If operation was not coalesced, then adde it to the queue.
        // If it was coalesced, then it was already in the queue.
        if coalescedOperation == nil {
            
            operationQueue.addOperation(operation)
        }
    }
    
    /**
     Function tries to coalesce an new added operation.
     
     - Parameter newOperation: the new operation to be added.
     - Parameter operationQueue: the operation queue where we are trying to add a new operation.
     
     - Returns: The operation with whom the method coalesced the new operation. Nil if the is no operation to coalesce.
     
     */
    fileprivate func coalesce(operation newOperation: Operation, operationQueue: OperationQueue) -> Operation? {
        
        let operations = operationQueue.operations as! [Operation]
        
        for operation in operations {
            
            let canAskToCoalesce = operation.isKind(of: Operation.self)
            
            if canAskToCoalesce &&
                operation.canCoalesceWithOperation(newOperation) {
                
                operation.coalesceWithOperation(newOperation)
                
                return operation
            }
        }
        
        return nil
    }
}
