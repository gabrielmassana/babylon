//
//  PostBodyCell.swift
//  Babylon
//
//  Created by Gabriel Massana on 29/10/2016.
//  Copyright © 2016 Gabriel Massana. All rights reserved.
//

import UIKit

class PostBodyCell: UITableViewCell, TableViewCell {
    
    //MARK: - Accessors
    
    var post: Post
    
    lazy var postBodyLabel: UILabel = {
        
        var postBodyLabel = UILabel.newAutoLayout()
        
        postBodyLabel.font = UIFont.tradeGothicLightWithSize(16.0)
        postBodyLabel.textAlignment = .left
        postBodyLabel.textColor = UIColor.scorpion()
        postBodyLabel.numberOfLines = 0
        
        return postBodyLabel
    }()
    
    //MARK: - Init
    
    init(post: Post) {
        
        self.post = post
        
        super.init(style: .default,
                   reuseIdentifier: nil)
        
        selectionStyle = .none
        
        contentView.addSubview(postBodyLabel)
        
        configureWithPost(post)
    }
    
    required init?(coder aDecoder: NSCoder) {
        
        fatalError("init(coder:) has not been implemented")
    }
    
    //MARK: - Constraints
    
    override func updateConstraints() {
        
        postBodyLabel.autoPinEdge(toSuperviewEdge: .left,
                                  withInset: 15.0 * DeviceManager.sharedInstance.resizeFactor)
        
        postBodyLabel.autoPinEdge(toSuperviewEdge: .right,
                                  withInset: 15.0 * DeviceManager.sharedInstance.resizeFactor)
        
        postBodyLabel.autoPinEdge(toSuperviewEdge: .top,
                                  withInset: 10.0 * DeviceManager.sharedInstance.resizeFactor)
        
        NSLayoutConstraint.autoSetPriority(UILayoutPriorityDefaultHigh) {
            
            postBodyLabel.autoPinEdge(toSuperviewEdge: .bottom,
                                      withInset: 10.0 * DeviceManager.sharedInstance.resizeFactor)
        }
        
        /*-------------------*/
        
        super.updateConstraints()
    }
    
    //MARK: - ConfigureCell
    
    /**
     Configure the self cell based on the post data.
     
     - Parameter post: The post with the data related to the cell.
     */
    private func configureWithPost(_ post: Post) {
        
        self.post = post
        
        postBodyLabel.text = post.body
    }
}
