//
//  Request.swift
//  Babylon
//
//  Created by Gabriel Massana on 26/10/2016.
//  Copyright © 2016 Gabriel Massana. All rights reserved.
//

import Alamofire

enum Request: URLRequestConvertible {
    
    case posts()
    case users()
    case comments()
    
    static let baseURLString = APIConfig.sharedInstance.APIHost
    
    var method: HTTPMethod {
        
        switch self {
            
        case .posts,
             .users,
             .comments:
            
            return .get
        }
    }

    var path: String {
        
        switch self {
            
        case .posts:
            
            return "/posts"
            
        case .users():
            
            return "/users"
            
        case .comments():
            
            return "/comments"
        }
    }
    
    // MARK: URLRequestConvertible
    
    func asURLRequest() throws -> URLRequest {
        
        let url = try Request.baseURLString.asURL()
        
        var urlRequest = URLRequest(url: url.appendingPathComponent(path))
        urlRequest.httpMethod = method.rawValue
        
        return urlRequest
    }
}
