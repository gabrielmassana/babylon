//
//  CompanyParser.swift
//  Babylon
//
//  Created by Gabriel Massana on 27/10/2016.
//  Copyright © 2016 Gabriel Massana. All rights reserved.
//

import UIKit

import CoreDataFullStack

class CompanyParser: Parser {

    func parseCompany(_ serverResponse: [String : AnyObject], userID: String) -> Company? {
        
        // Serializer
        guard let companySerializer = CompanySerializer(serverResponse: serverResponse),
            let parserManagedObjectContext = parserManagedObjectContext else {
                
                return nil
        }
        
        // Fetch Company
        var company = Company.fetchCompany(userID,
                                           managedObjectContext: parserManagedObjectContext)
        
        if company == nil {
            
            company = CDFInsertService.insertNewObject(forEntityClass: Company.self,
                                                       in: parserManagedObjectContext) as? Company
        }
        
        company?.bs = companySerializer.bs
        company?.catchPhrase = companySerializer.catchPhrase
        company?.name = companySerializer.name

        return company
    }
}
