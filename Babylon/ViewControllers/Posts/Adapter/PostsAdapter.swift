//
//  PostsAdapter.swift
//  Babylon
//
//  Created by Gabriel Massana on 27/10/2016.
//  Copyright © 2016 Gabriel Massana. All rights reserved.
//

import UIKit

import CoreDataFullStack
import TableViewDataStatus

protocol PostsAdapterDelegate: class {
    
    func didSelectPost(post: Post)
}

class PostsAdapter: NSObject {

    //MARK: - Accessors

    /// Delegate object
    weak var delegate: PostsAdapterDelegate?
    
    var heightAtIndexPath = [IndexPath : NSNumber]()
    
    /// Table view to display data.
    var tableView: TableViewDataStatus? {
        
        willSet (newValue) {
            
            if newValue != tableView {
                
                self.tableView = newValue
                
                tableView?.dataSource = self
                tableView?.delegate = self
                tableView?.backgroundColor = UIColor.white
                tableView?.separatorStyle = .none
                
                tableView?.rowHeight = UITableViewAutomaticDimension
                tableView?.estimatedRowHeight = UITableViewAutomaticDimension
                
                // RegisterCells
                registerCells()
                
                // FRC performFetch
                let _ = try? self.fetchedResultsController.performFetch()
            }
        }
    }
    
    /// Used to connect the TableView with Core Data.
    lazy var fetchedResultsController: TableViewFetchedResultsController =  {
        
        let fetchedResultsController = TableViewFetchedResultsController(fetchRequest: self.fetchRequest,
                                                                         managedObjectContext: CDFCoreDataManager.sharedInstance().managedObjectContext,
                                                                         sectionNameKeyPath: nil,
                                                                         cacheName: nil)
        
        fetchedResultsController.tableView = self.tableView
        fetchedResultsController.deleteRowAnimation = .none
        fetchedResultsController.frcDelegate = self
        fetchedResultsController.disableAnimations = true
        
        return fetchedResultsController
    }()
    
    /// Fetch request for posts.
    lazy var fetchRequest: NSFetchRequest<Post> = {
        
        let fetchRequest = NSFetchRequest<Post>()
        
        fetchRequest.entity =  NSEntityDescription.entity(forEntityName: String.init(describing: Post.self),
                                                          in: CDFCoreDataManager.sharedInstance().managedObjectContext)
        
        fetchRequest.sortDescriptors = self.sortDescriptors
        
        return fetchRequest
    }()
    
    /// Sort Descriptors to sort how posts should be ordered.
    lazy var sortDescriptors: [NSSortDescriptor] = {
        
        let sortDescriptors: NSSortDescriptor = NSSortDescriptor(key: "postID",
                                                                 ascending: true)
        
        return [sortDescriptors]
    }()
    
    //MARK: - Init
    
    override init() {
        
        super.init()
    }
    
    //MARK: - RegisterCells
    
    /**
     Register the cells to be used in the table view.
     */
    func registerCells() {
        
        tableView?.register(PostCell.self,
                            forCellReuseIdentifier: PostCell.reuseIdentifier())
    }
    
    //MARK: - ConfigureCell
    
    /**
     Configure the cell.
     
     - Parameter cell: cell to be configured.
     - Parameter indexPath: cell indexPath.
     */
    func configureCell(_ cell: UITableViewCell, indexPath: IndexPath) {
        
        guard let fetchedObjects = fetchedResultsController.fetchedObjects else {
                
                return
        }
        
        let post = fetchedObjects[(indexPath as NSIndexPath).row]
        
        let cell = cell as! PostCell
        cell.configureWithPost(post)
        cell.layoutByApplyingConstraints()
        cell.rasterizeLayer()
    }
    
    //MARK: - Refresh
    
    func refresh() {
        
        refreshComments()
    }

    func refreshComments() {
        
        tableView?.willLoadContent()

        CommentsAPIManager.retrieveComments(success:
            { (result) in
                
                self.refreshUsers()
                
        }) { (error) in

            self.refreshUsers()
        }
    }
    
    func refreshUsers() {
        
        UsersAPIManager.retrieveUsers(success:
            { (result) in
                
                self.refreshPosts()
                
        }) { (error) in
            
            self.refreshPosts()
        }
    }
    
    func refreshPosts() {
        
        PostsAPIManager.retrievePosts(success:
            { (result) in
                
                self.tableView?.didFinishLoadingContent(false)

        }) { (error) in
            
            self.tableView?.didFinishLoadingContent(false)
        }
    }
}

extension PostsAdapter: UITableViewDataSource {
    
    //MARK: - UITableViewDataSource
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        guard let count = fetchedResultsController.fetchedObjects?.count else {
            
            return 0
        }
        
        return count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: PostCell.reuseIdentifier(),
                                                 for: indexPath)
        
        configureCell(cell,
                      indexPath: indexPath)
        
        return cell
    }
}

extension PostsAdapter: UITableViewDelegate {
    
    //MARK: - UITableViewDelegate
    
    func tableView(_ tableView: UITableView,
                   estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        
        let height = heightAtIndexPath[indexPath]
        
        if let height = height {
            
            return CGFloat(height)
        }
        else {
            
            return UITableViewAutomaticDimension
        }
    }
    
    func tableView(_ tableView: UITableView,
                   willDisplay cell: UITableViewCell,
                   forRowAt indexPath: IndexPath) {
        
        let height: NSNumber = NSNumber(value: Float(cell.frame.height))
        heightAtIndexPath[indexPath] = height
    }
    
    func tableView(_ tableView: UITableView,
                   didEndDisplaying cell: UITableViewCell,
                   forRowAt indexPath: IndexPath) {
        
        let height: NSNumber = NSNumber(value: Float(cell.frame.height))
        heightAtIndexPath[indexPath] = height
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        guard let fetchedObjects = fetchedResultsController.fetchedObjects else {
            
            return
        }
        
        let post = fetchedObjects[(indexPath as NSIndexPath).row]
    
        self.delegate?.didSelectPost(post: post)
    }
}

extension PostsAdapter: TableViewFetchedResultsControllerDelegate {
    
    //MARK: - TableViewFetchedResultsControllerDelegate
    
    func didUpdateCell(_ indexPath: IndexPath) {
        
        guard let cell = tableView?.cellForRow(at: indexPath) else {
            
            return
        }
        
        self.configureCell(cell,
                           indexPath: indexPath)
    }
    
    func didUpdateContent() {
        
    }
}
