//
//  CommentsParserTests.swift
//  Babylon
//
//  Created by Gabriel Massana on 30/10/2016.
//  Copyright © 2016 Gabriel Massana. All rights reserved.
//

import XCTest

import CoreDataFullStack

@testable import Babylon

class CommentsParserTests: XCTestCase {
    
    //MARK: - Accessors
    
    var parser: CommentsParser?
    
    var commentsJSON: [[String : AnyObject]]?
    var commentJSON: [String : AnyObject]?
    
    var postID: NSNumber?
    var id: NSNumber?
    var commentName: String?
    var email: String?
    var body: String?
    
    // MARK: - TestSuiteLifecycle
    
    override func setUp() {
        
        super.setUp()
        
        parser = CommentsParser.parser(CDFCoreDataManager.sharedInstance().managedObjectContext)
        
        postID = 1
        id = 1
        commentName = "id labore ex et quam laborum"
        email = "Eliseo@gardner.biz"
        body = "laudantium enim quasi est quidem magnam voluptate ipsam eos\ntempora quo necessitatibus\ndolor quam autem quasi\nreiciendis et nam sapiente accusantium"
        
        commentJSON = [
            "postId": postID!,
            "id": id!,
            "name": commentName! as AnyObject,
            "email": email! as AnyObject,
            "body": body! as AnyObject
        ]
        
        commentsJSON = [
            commentJSON!,
            [
                "postId": 1 as NSNumber,
                "id": 2 as NSNumber,
                "name": "quo vero reiciendis velit similique earum" as AnyObject,
                "email": "Jayne_Kuhic@sydney.com" as AnyObject,
                "body": "est natus enim nihil est dolore omnis voluptatem numquam\net omnis occaecati quod ullam at\nvoluptatem error expedita pariatur\nnihil sint nostrum voluptatem reiciendis et" as AnyObject
            ],
            [
                "postId": 1 as NSNumber,
                "id": 3 as NSNumber,
                "name": "odio adipisci rerum aut animi" as AnyObject,
                "email": "Nikita@garfield.biz" as AnyObject,
                "body": "quia molestiae reprehenderit quasi aspernatur\naut expedita occaecati aliquam eveniet laudantium\nomnis quibusdam delectus saepe quia accusamus maiores nam est\ncum et ducimus et vero voluptates excepturi deleniti ratione" as AnyObject
            ]
        ]
    }
    
    override func tearDown() {
        
        CDFCoreDataManager.sharedInstance().reset()
        
        parser = nil
        
        postID = nil
        id = nil
        commentName = nil
        email = nil
        body = nil
        
        commentJSON = nil
        commentsJSON = nil
        
        super.tearDown()
    }

    // MARK: - CommentsParser
    
    func test_commentsParser_newCommentObjectReturned() {
        
        let comments = parser?.parseComments(commentsJSON!)
        
        XCTAssertNotNil(comments, "A valid Comments Array object wasn't created")
    }
    
    func test_commentsParser_count() {
        
        let comments = parser?.parseComments(commentsJSON!)
        
        let arrayCount = NSNumber(value: comments!.count)
        let jsonCount = NSNumber(value: commentsJSON!.count as Int)
        
        XCTAssertTrue(arrayCount == jsonCount, String(format:"Parsed count is wrong. Should be %@ rather than: %@", arrayCount, jsonCount))
    }
    
    func test_parseComments_uniqueObjects() {
        
        let comments = parser?.parseComments(commentsJSON!)
        
        let firstObject = comments!.first!
        let lastObject = comments!.last!
        
        XCTAssertNotEqual(firstObject, lastObject, String(format:"Parsed objects are wrong: %@ and %@", firstObject, lastObject))
    }
    
    func test_parseComments_firstObject_commentID() {
        
        let comments = parser?.parseComments(commentsJSON!)
        
        let firstObject = comments!.first!
        
        XCTAssertEqual(firstObject.commentID, id!.stringValue, String(format:"Parsed object is wrong: %@", firstObject))
    }
    
    func test_parseComments_firstObject_name() {
        
        let comments = parser?.parseComments(commentsJSON!)
        
        let firstObject = comments!.first!
        
        XCTAssertEqual(firstObject.name, commentName!, String(format:"Parsed object is wrong: %@", firstObject))
    }
    
    func test_parseComments_firstObject_email() {
        
        let comments = parser?.parseComments(commentsJSON!)
        
        let firstObject = comments!.first!
        
        XCTAssertEqual(firstObject.email, email!, String(format:"Parsed object is wrong: %@", firstObject))
    }
    
    func test_parseComments_firstObject_body() {
        
        let comments = parser?.parseComments(commentsJSON!)
        
        let firstObject = comments!.first!
        
        XCTAssertEqual(firstObject.body, body!, String(format:"Parsed object is wrong: %@", firstObject))
    }
    
    func test_parseComments_firstObject_postID() {
        
        let comments = parser?.parseComments(commentsJSON!)
        
        let firstObject = comments!.first!
        
        XCTAssertEqual(firstObject.post!.postID, postID!.stringValue, String(format:"Parsed object is wrong: %@", firstObject))
    }
}
