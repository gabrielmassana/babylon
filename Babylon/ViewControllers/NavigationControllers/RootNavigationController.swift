//
//  RootNavigationController.swift
//  Babylon
//
//  Created by Gabriel Massana on 26/10/2016.
//  Copyright © 2016 Gabriel Massana. All rights reserved.
//

import UIKit

class RootNavigationController: UINavigationController {
    
    //MARK: - Accessors
    
    /// RootViewController for the Navigation Controller.
    let rootViewController: UIViewController = {
        
        return PostsViewController()
    }()
    
    //MARK: - Init
    
    init() {
        
        super.init(nibName: nil, bundle: nil)
        
        viewControllers = [rootViewController]
    }
    
    required init?(coder aDecoder: NSCoder) {
        
        fatalError("init(coder:) has not been implemented")
    }
}
