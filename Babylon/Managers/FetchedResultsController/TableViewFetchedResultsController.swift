//
//  TableViewFetchedResultsController.swift
//  Babylon
//
//  Created by Gabriel Massana on 27/10/2016.
//  Copyright © 2016 Gabriel Massana. All rights reserved.
//

import UIKit

import CoreData

protocol TableViewFetchedResultsControllerDelegate: class {
    
    /**
     Informational call for when the FRC updates.
     */
    func didUpdateContent()
    
    /**
     Informational call for when the FRC updates.
     
     - param indexPath: indexPath to the updated cell.
     */
    func didUpdateCell(_ indexPath: IndexPath)
}

class TableViewFetchedResultsController: NSFetchedResultsController<Post> {
    
    //MARK: Accessors
    
    /// TableViewFetchedResultsControllerDelegate delegate object
    weak var frcDelegate: TableViewFetchedResultsControllerDelegate?
    
    /// Table view for the fetch result controller to update.
    weak var tableView: UITableView?
    
    /// Specifies if the fetch result controller should update it's sections.
    var shouldUpdateSections: Bool = true
    
    /// Disables all animations when updating table view.
    var disableAnimations = false
    
    /**
     Animation effect on an insert row action.
     */
    var insertRowAnimation: UITableViewRowAnimation = UITableViewRowAnimation.automatic
    
    /**
     Animation effect on a delete row action.
     */
    var deleteRowAnimation: UITableViewRowAnimation = UITableViewRowAnimation.automatic
    
    /**
     Animation effect on an insert section action.
     */
    var insertSectionAnimation: UITableViewRowAnimation = UITableViewRowAnimation.automatic
    
    /**
     Animation effect on a delete section action.
     */
    var deleteSectionAnimation: UITableViewRowAnimation = UITableViewRowAnimation.automatic
    
    //MARK: Init
    
    override init(fetchRequest: NSFetchRequest<Post>, managedObjectContext context: NSManagedObjectContext, sectionNameKeyPath: String?, cacheName name: String?) {
        
        super.init(fetchRequest: fetchRequest,
                   managedObjectContext: context,
                   sectionNameKeyPath: sectionNameKeyPath,
                   cacheName: name)
        
        self.delegate = self
    }
}

extension TableViewFetchedResultsController: NSFetchedResultsControllerDelegate {
    
    //MARK: NSFetchedResultsControllerDelegate
    
    func controllerWillChangeContent(_ controller: NSFetchedResultsController<NSFetchRequestResult>) {
        
        self.tableView?.beginUpdates()
    }
    
    func controller(_ controller: NSFetchedResultsController<NSFetchRequestResult>,
                    didChange anObject: Any,
                    at indexPath: IndexPath?,
                    for type: NSFetchedResultsChangeType,
                    newIndexPath: IndexPath?) {
        
        switch type {
            
        case NSFetchedResultsChangeType.insert:
            
            if let newIndexPath = newIndexPath {
                
                self.tableView?.insertRows(at: [newIndexPath],
                                           with: self.insertRowAnimation)
            }
            
        case NSFetchedResultsChangeType.delete:
            
            if let indexPath = indexPath {
                
                self.tableView?.deleteRows(at: [indexPath],
                                           with: self.deleteRowAnimation)
            }
            
        case NSFetchedResultsChangeType.update:
            
            if let indexPath = indexPath {
                
                self.frcDelegate?.didUpdateCell(indexPath)
            }
            
        case NSFetchedResultsChangeType.move:
            
            if let newIndexPath = newIndexPath,
                let indexPath = indexPath {
                
                self.tableView?.deleteRows(at: [indexPath],
                                           with: self.deleteRowAnimation)
                
                self.tableView?.insertRows(at: [newIndexPath],
                                           with: self.insertRowAnimation)
            }
        }
    }
    
    func controller(_ controller: NSFetchedResultsController<NSFetchRequestResult>,
                    didChange sectionInfo: NSFetchedResultsSectionInfo,
                    atSectionIndex sectionIndex: Int,
                    for type: NSFetchedResultsChangeType) {
        
        if shouldUpdateSections {
            
            switch type {
            case NSFetchedResultsChangeType.insert:
                
                self.tableView?.insertSections(IndexSet(integer: sectionIndex),
                                               with: self.insertSectionAnimation)
                
            case NSFetchedResultsChangeType.delete:
                
                self.tableView?.deleteSections(IndexSet(integer: sectionIndex),
                                               with: self.deleteRowAnimation)
                
            case NSFetchedResultsChangeType.move,
                 NSFetchedResultsChangeType.update:
                
                ()
            }
        }
    }
    
    func controllerDidChangeContent(_ controller: NSFetchedResultsController<NSFetchRequestResult>) {
        
        var contentOffset = CGPoint.zero
        
        if disableAnimations == true {
            
            contentOffset = tableView!.contentOffset
            UIView.setAnimationsEnabled(false)
        }
        
        tableView?.endUpdates()
        
        if disableAnimations == true {
            
            UIView.setAnimationsEnabled(true)
            tableView?.contentOffset = contentOffset
        }
        
        frcDelegate?.didUpdateContent()
    }
}
