//
//  User+CoreDataClass.swift
//  
//
//  Created by Gabriel Massana on 26/10/2016.
//
//

import Foundation

import CoreData
import CoreDataFullStack

@objc(User)
public class User: NSManagedObject {
    
    class func fetchUser(_ userID: String, managedObjectContext: NSManagedObjectContext) -> User? {
        
        let predicate = NSPredicate(format: "userID == %@", userID)
        
        let user = CDFRetrievalService.retrieveFirstEntry(forEntityClass: User.self,
                                                          predicate: predicate,
                                                          managedObjectContext: managedObjectContext) as? User
        
        return user
    }
}
