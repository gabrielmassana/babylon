//
//  PostsViewController.swift
//  Babylon
//
//  Created by Gabriel Massana on 27/10/2016.
//  Copyright © 2016 Gabriel Massana. All rights reserved.
//

import UIKit

import CoreDataFullStack
import PureLayout
import TableViewDataStatus

class PostsViewController: UIViewController {

    //MARK: - Accessors
    
    /// Table view to display the posts.
    lazy var tableView: TableViewDataStatus = {
        
        let tableView: TableViewDataStatus = TableViewDataStatus.newAutoLayout()
        
        tableView.emptyView = self.emptyView
        tableView.loadingView = self.loadingView
        
        return tableView
    }()
    
    /// Empty View to be shown when no data in the Table View.
    lazy var emptyView: EmptyView = {
        
        let emptyView = EmptyView(frame: CGRect.init(x: 0.0,
                                                     y: 0.0,
                                                     width: UIScreen.main.bounds.width,
                                                     height: UIScreen.main.bounds.height))
        
        return emptyView
    }()
    
    /// Empty View to be shown when no data in the Table View.
    lazy var loadingView: LoadingView = {
        
        let loadingView = LoadingView(frame: CGRect.init(x: 0.0,
                                                         y: 0.0,
                                                         width: UIScreen.main.bounds.width,
                                                         height: UIScreen.main.bounds.height))
        
        return loadingView
    }()
    
    /// Adapter to manage the common logic and data of the tableView.
    lazy var adapter: PostsAdapter = {
        
        let adapter = PostsAdapter()
        
        adapter.delegate = self
        
        return adapter
    }()
    
    lazy var navigationItemTitleView: NavigationItemTitleView = {
        
        let navigationItemTitleView = NavigationItemTitleView(labelText: NSLocalizedString("Posts", comment: "posts_title"))
        
        return navigationItemTitleView
    }()
    
    //MARK: - ViewLifeCycle

    override func viewDidLoad() {
        
        super.viewDidLoad()

        view.addSubview(tableView)
        adapter.tableView = tableView
        
        /*-------------------*/

        updateViewConstraints()
        
        /*-------------------*/
        
        view.backgroundColor = UIColor.alto()
        
        navigationItem.titleView = navigationItemTitleView
        
        adapter.refresh()
    }
    
    //MARK: - Constraints
    
    override func updateViewConstraints() {
        
        super.updateViewConstraints()
        
        /*-------------------*/
        
        tableView.autoPinEdgesToSuperviewEdges()
    }
}

extension PostsViewController : PostsAdapterDelegate {
    
    //MARK: - PostsAdapterDelegate

    internal func didSelectPost(post: Post) {
        
        // Push to Details
        
        navigationController?.pushViewController(DetailsViewController(post: post),
                                                 animated: true)
    }
}
