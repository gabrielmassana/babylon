//
//  NetworkingManager.swift
//  Babylon
//
//  Created by Gabriel Massana on 26/10/2016.
//  Copyright © 2016 Gabriel Massana. All rights reserved.
//

import UIKit

import Alamofire

/// Manager to handle networking based on Alamofire.
class NetworkingManager: NSObject {
    
    //MARK: - Accessors
    
    /// Networking manager with Default Session Configuration
    static var defaultManager: Alamofire.SessionManager = {
        
        let configuration = URLSessionConfiguration.default
        configuration.httpAdditionalHeaders = Alamofire.SessionManager.defaultHTTPHeaders
        
        let defaultManager = Alamofire.SessionManager(configuration: configuration)
        
        return defaultManager
    }()
    
    //MARK: - Init
    
    override init() {
        
        super.init()
    }
}
