//
//  UserCompanyCell.swift
//  Babylon
//
//  Created by Gabriel Massana on 29/10/2016.
//  Copyright © 2016 Gabriel Massana. All rights reserved.
//

import UIKit

class UserCompanyCell: UITableViewCell, TableViewCell {
    
    //MARK: - Accessors
    
    var post: Post
    
    lazy var userCompanyLabel: UILabel = {
        
        var userCompanyLabel = UILabel.newAutoLayout()
        
        userCompanyLabel.font = UIFont.tradeGothicLTWithSize(16.0)
        userCompanyLabel.textAlignment = .left
        userCompanyLabel.textColor = UIColor.black
        userCompanyLabel.numberOfLines = 0
        
        return userCompanyLabel
    }()
    
    //MARK: - Init
    
    init(post: Post) {
        
        self.post = post
        
        super.init(style: .default,
                   reuseIdentifier: nil)
        
        selectionStyle = .none
        
        contentView.addSubview(userCompanyLabel)
        
        configureWithPost(post)
    }
    
    required init?(coder aDecoder: NSCoder) {
        
        fatalError("init(coder:) has not been implemented")
    }
    
    //MARK: - Constraints
    
    override func updateConstraints() {
        
        userCompanyLabel.autoPinEdge(toSuperviewEdge: .left,
                                     withInset: 15.0 * DeviceManager.sharedInstance.resizeFactor)
        
        userCompanyLabel.autoPinEdge(toSuperviewEdge: .right,
                                     withInset: 15.0 * DeviceManager.sharedInstance.resizeFactor)
        
        userCompanyLabel.autoPinEdge(toSuperviewEdge: .top,
                                     withInset: 10.0 * DeviceManager.sharedInstance.resizeFactor)
        
        NSLayoutConstraint.autoSetPriority(UILayoutPriorityDefaultHigh) {
            
            userCompanyLabel.autoPinEdge(toSuperviewEdge: .bottom,
                                         withInset: 10.0 * DeviceManager.sharedInstance.resizeFactor)
        }
        
        /*-------------------*/
        
        super.updateConstraints()
    }
    
    //MARK: - ConfigureCell
    
    /**
     Configure the self cell based on the post data.
     
     - Parameter post: The post with the data related to the cell.
     */
    private func configureWithPost(_ post: Post) {
        
        self.post = post
        
        guard let user = post.user,
            let company = user.company,
            let name = company.name,
            let catchPhrase = company.catchPhrase,
            let bs = company.bs else {
                
                return
        }
        
        let nameTitle = "\(NSLocalizedString("Name: ", comment: "name_title"))\(name)\n"
        let catchPhraseTitle = "\(NSLocalizedString("Catch Phrase: ", comment: "catch_phrase_title"))\(catchPhrase)\n"
        let bsTitle = "\(NSLocalizedString("Bs: ", comment: "bs_title"))\(bs)"
        
        userCompanyLabel.text = nameTitle + catchPhraseTitle + bsTitle
    }
}
