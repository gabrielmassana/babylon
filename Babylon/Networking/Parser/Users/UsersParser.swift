//
//  UsersParser.swift
//  Babylon
//
//  Created by Gabriel Massana on 26/10/2016.
//  Copyright © 2016 Gabriel Massana. All rights reserved.
//

import UIKit

import CoreDataFullStack

class UsersParser: Parser {
    
    func parseUsers(_ serverResponse: [[String : AnyObject]]) -> [User] {
        
        var users = [User]()
        
        for serverUser in serverResponse {
            
            let user = parseUser(serverUser)
            
            if let user = user {
                
                users.append(user)
            }
        }
        
        return users
    }
    
    fileprivate func parseUser(_ serverResponse: [String : AnyObject]) -> User? {
        
        // Serializer
        guard let userSerializer = UserSerializer(serverResponse: serverResponse),
            let parserManagedObjectContext = parserManagedObjectContext else {
                
                return nil
        }
        
        // Fetch User
        var user = User.fetchUser(userSerializer.userID,
                                  managedObjectContext: parserManagedObjectContext)
        
        if user == nil {
            
            user = CDFInsertService.insertNewObject(forEntityClass: User.self,
                                                    in: parserManagedObjectContext) as? User
            
            user?.userID = userSerializer.userID
        }
        
        user?.email = userSerializer.email
        user?.name = userSerializer.name
        user?.phone = userSerializer.phone
        user?.username = userSerializer.username
        user?.website = userSerializer.website
        
        // Parse Address
        if let userAddress = userSerializer.address {
            
            let addressParser = AddressParser.parser(parserManagedObjectContext)
            
            let address = addressParser.parseAddress(userAddress,
                                                     userID: userSerializer.userID)
            
            user?.address = address
        }
        
        // Parse Company
        if let userCompany = userSerializer.company {
            
            let companyParser = CompanyParser.parser(parserManagedObjectContext)
            
            let company = companyParser.parseCompany(userCompany,
                                                     userID: userSerializer.userID)
            
            user?.company = company
        }
        
        return user
    }
}
