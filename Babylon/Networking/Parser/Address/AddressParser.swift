//
//  AddressParser.swift
//  Babylon
//
//  Created by Gabriel Massana on 27/10/2016.
//  Copyright © 2016 Gabriel Massana. All rights reserved.
//

import UIKit

import CoreDataFullStack

class AddressParser: Parser {

    func parseAddress(_ serverResponse: [String : AnyObject], userID: String) -> Address? {
        
        // Serializer
        guard let addressSerializer = AddressSerializer(serverResponse: serverResponse),
            let parserManagedObjectContext = parserManagedObjectContext else {
                
                return nil
        }
        
        // Fetch Address
        var address = Address.fetchAddress(userID,
                                           managedObjectContext: parserManagedObjectContext)
        
        if address == nil {

            address = CDFInsertService.insertNewObject(forEntityClass: Address.self,
                                                       in: parserManagedObjectContext) as? Address
        }
        
        address?.city = addressSerializer.city
        address?.latitude = addressSerializer.latitude
        address?.longitude = addressSerializer.longitude
        address?.street = addressSerializer.street
        address?.suite = addressSerializer.suite
        address?.zipcode = addressSerializer.zipcode
        
        return address
    }
}
