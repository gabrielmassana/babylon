//
//  ErrorSerializer.swift
//  Babylon
//
//  Created by Gabriel Massana on 26/10/2016.
//  Copyright © 2016 Gabriel Massana. All rights reserved.
//

import UIKit

public struct BabylonError {
    
    /// The domain used for creating all Babylon errors.
    public static let Domain = "com.babylon.error"
}

enum BabylonErrorCode: Int {
    
    case connectionBabylonError = -1007
    case unknownBabylonError = -1
}

class ErrorSerializer: NSObject {
    
    class func serializeError(_ error: NSError?, data: Data?) -> NSError {
        
        var returnError: NSError
        
        if error?.code == URLError.notConnectedToInternet.rawValue ||
            error?.code == URLError.cannotFindHost.rawValue ||
            error?.code == URLError.cannotConnectToHost.rawValue ||
            error?.code == URLError.networkConnectionLost.rawValue ||
            error?.code == URLError.timedOut.rawValue {
            
            returnError = NSError(domain: BabylonError.Domain,
                                  code: BabylonErrorCode.connectionBabylonError.rawValue,
                                  userInfo: [
                                    NSLocalizedDescriptionKey : "There was a problem with your internet connection, please try again later.",
                                    NSUnderlyingErrorKey : error
                ])
        }
        else {
                
          returnError = unknownError
            
        }
        
        return returnError
    }
    
    static let unknownError: NSError = {
        
        return NSError(domain: BabylonError.Domain,
                       code: BabylonErrorCode.unknownBabylonError.rawValue,
                       userInfo: [
                        NSLocalizedDescriptionKey : "Unknown error"
            ])
    }()
}
