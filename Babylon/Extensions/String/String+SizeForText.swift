//
//  String+SizeForText.swift
//  Babylon
//
//  Created by Gabriel Massana on 27/10/2016.
//  Copyright © 2016 Gabriel Massana. All rights reserved.
//

import UIKit

/// Helps calculate the size of a string.
extension String {
    
    /**
     Calculate the size of a string depending on its font, text and container size.
     
     - Parameter font: The font of the string text.
     - Parameter boundingRectSize: The size of the rectangle where the text should fit.
     
     - Returns: The size of the text.
     */
    func sizeForText(_ font: UIFont, boundingRectSize: CGSize) -> CGSize {
        
        let textRect = self.boundingRect(with: boundingRectSize,
                                         options: [
                                            NSStringDrawingOptions.usesLineFragmentOrigin,
                                            NSStringDrawingOptions.usesFontLeading],
                                         attributes: [NSFontAttributeName : font],
                                         context: nil)
        
        return textRect.size
    }
}
