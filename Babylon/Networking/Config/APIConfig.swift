//
//  APIConfig.swift
//  Babylon
//
//  Created by Gabriel Massana on 26/10/2016.
//  Copyright © 2016 Gabriel Massana. All rights reserved.
//

import UIKit

class APIConfig: NSObject {

    //MARK: - Singleton
    
    /// Returns the global APIConfig instance.
    static let sharedInstance = APIConfig()

    /// Base path to the API.
    let APIHost: String = {
        
        return "http://jsonplaceholder.typicode.com"
    }()
}
