//
//  UIFont+Babylon.swift
//  Babylon
//
//  Created by Gabriel Massana on 27/10/2016.
//  Copyright © 2016 Gabriel Massana. All rights reserved.
//

import UIKit

/**
 Accessors for all the fonts used.
 */
extension UIFont {
    
    class func tradeGothicLightWithSize(_ size: CGFloat) -> UIFont {
        
        return UIFont(name: "TradeGothic-Light",
                      size: size * DeviceManager.sharedInstance.resizeFactor)!
    }
    
    class func tradeGothicLTWithSize(_ size: CGFloat) -> UIFont {
        
        return UIFont(name: "TradeGothicLT",
                      size: size * DeviceManager.sharedInstance.resizeFactor)!
    }
    
    class func tradeGothicNo2BoldWithSize(_ size: CGFloat) -> UIFont {
        
        return UIFont(name: "TradeGothicNo.2-Bold",
                      size: size * DeviceManager.sharedInstance.resizeFactor)!
    }
}
