//
//  APIManager.swift
//  Babylon
//
//  Created by Gabriel Massana on 26/10/2016.
//  Copyright © 2016 Gabriel Massana. All rights reserved.
//

import UIKit

typealias NetworkingOnSuccess = (_ result: AnyObject?) -> Void
typealias NetworkingOnFailure = (_ error: NSError?) -> Void
typealias NetworkingOnCompletion = (_ result: AnyObject?) -> Void

///  Abstract API Manager instance with callback accessors.
class APIManager: NSObject {
    
    //MARK: - Accessors
    
    /// Callback called when the networking operation completes succesfully.
    var onSuccess: NetworkingOnSuccess?
    
    /// Callback called when the networking operation fails.
    var onFailure: NetworkingOnFailure?
    
    /// Callback called when the networking operation completes.
    var onCompletion: NetworkingOnCompletion?
}
