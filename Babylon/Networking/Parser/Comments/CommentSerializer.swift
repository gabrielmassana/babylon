//
//  CommentSerializer.swift
//  Babylon
//
//  Created by Gabriel Massana on 27/10/2016.
//  Copyright © 2016 Gabriel Massana. All rights reserved.
//

import Foundation

struct CommentSerializer {
    
    var postID: String
    var commentID: String //id
    
    var body: String?
    var name: String?
    var email: String?
    
    init?(serverResponse: [String : AnyObject]) {
        
        guard let commentID = serverResponse["id"] as? NSNumber,
            let postID = serverResponse["postId"] as? NSNumber else {
                
                return nil
        }
        
        self.postID = postID.stringValue
        self.commentID = commentID.stringValue
        
        if let body = serverResponse["body"] as? String {
            
            self.body = body
        }
        
        if let name = serverResponse["name"] as? String {
            
            self.name = name
        }
        
        if let email = serverResponse["email"] as? String {
            
            self.email = email
        }
    }
}
