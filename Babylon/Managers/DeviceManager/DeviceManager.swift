//
//  DeviceManager.swift
//  Babylon
//
//  Created by Gabriel Massana on 27/10/2016.
//  Copyright © 2016 Gabriel Massana. All rights reserved.
//

import UIKit

let iPhone5Width: CGFloat = 320.0

/// The singleton returns the factor to resize iPhone 6/7 and iPhone 6/7+ to the correct assets sizes.
class DeviceManager: NSObject {
    
    /**
     Singleton.
     
     - Returns: DeviceManager instance
     */
    static let sharedInstance = DeviceManager()
    
    /// The factor should be used to resize assets and fonts.
    let resizeFactor: CGFloat = UIScreen.main.bounds.size.width / iPhone5Width
}
