//
//  UserSerializer.swift
//  Babylon
//
//  Created by Gabriel Massana on 27/10/2016.
//  Copyright © 2016 Gabriel Massana. All rights reserved.
//

import Foundation

struct UserSerializer {

    var userID: String


    var email: String?
    var name: String?
    var phone: String?
    var username: String?
    var website: String?

    var address: [String : AnyObject]?
    var company: [String : AnyObject]?
    
    init?(serverResponse: [String : AnyObject]) {
        
        guard let userID = serverResponse["id"] as? NSNumber else {
                
                return nil
        }

        self.userID = userID.stringValue
        
        if let address = serverResponse["address"] as? [String : AnyObject] {
            
            self.address = address
        }
        
        if let company = serverResponse["company"] as? [String : AnyObject] {
            
            self.company = company
        }

        if let email = serverResponse["email"] as? String {
            
            self.email = email
        }
        
        if let name = serverResponse["name"] as? String {
            
            self.name = name
        }
        
        if let phone = serverResponse["phone"] as? String {
            
            self.phone = phone
        }
        
        if let username = serverResponse["username"] as? String {
            
            self.username = username
        }
        
        if let website = serverResponse["website"] as? String {
            
            self.website = website
        }
    }
}
