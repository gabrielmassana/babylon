//
//  Comment+CoreDataProperties.swift
//  
//
//  Created by Gabriel Massana on 27/10/2016.
//
//

import Foundation
import CoreData

extension Comment {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<Comment> {
        return NSFetchRequest<Comment>(entityName: "Comment");
    }

    @NSManaged public var body: String?
    @NSManaged public var commentID: String?
    @NSManaged public var email: String?
    @NSManaged public var name: String?
    @NSManaged public var post: Post?

}
