//
//  CompanyParserTests.swift
//  Babylon
//
//  Created by Gabriel Massana on 30/10/2016.
//  Copyright © 2016 Gabriel Massana. All rights reserved.
//

import XCTest

import CoreDataFullStack

@testable import Babylon

class CompanyParserTests: XCTestCase {
    
    //MARK: - Accessors
    
    var parser: CompanyParser?
    
    var companyJSON: [String : AnyObject]?
    
    var userID: String?
    var companyName: String?
    var catchPhrase: String?
    var bs: String?
    
    // MARK: - TestSuiteLifecycle
    
    override func setUp() {
        
        super.setUp()
        
        parser = CompanyParser.parser(CDFCoreDataManager.sharedInstance().managedObjectContext)
        
        userID = "1"

        companyName =  "Romaguera-Crona"
        catchPhrase = "Multi-layered client-server neural-net"
        bs = "harness real-time e-markets"

        companyJSON = [
            "name" : companyName as AnyObject,
            "catchPhrase" : catchPhrase as AnyObject,
            "bs" : bs as AnyObject,
        ]
    }

    override func tearDown() {
        
        CDFCoreDataManager.sharedInstance().reset()
        
        parser = nil
        
        userID = nil
        companyName = nil
        catchPhrase = nil
        bs = nil
        
        companyJSON = nil
        
        super.tearDown()
    }
    
    // MARK: - CompanyParser
    
    func test_companyParser_newCompanyObjectReturned() {
        
        let company = parser?.parseCompany(companyJSON!,
                                           userID: userID!)
        
        XCTAssertNotNil(company, "A valid Company object wasn't created")
    }
    
    func test_parseCompany_name() {
        
        let company = parser?.parseCompany(companyJSON!,
                                           userID: userID!)
        
        XCTAssertEqual(company!.name, companyName!, String(format:"Parsed object is wrong: %@", company!))
    }
    
    func test_parseCompany_catchPhrase() {
        
        let company = parser?.parseCompany(companyJSON!,
                                           userID: userID!)
        
        XCTAssertEqual(company!.catchPhrase, catchPhrase!, String(format:"Parsed object is wrong: %@", company!))
    }
    
    func test_parseCompany_bs() {
        
        let company = parser?.parseCompany(companyJSON!,
                                           userID: userID!)
        
        XCTAssertEqual(company!.bs, bs!, String(format:"Parsed object is wrong: %@", company!))
    }
}
