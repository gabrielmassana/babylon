//
//  DetailsAdapter.swift
//  Babylon
//
//  Created by Gabriel Massana on 28/10/2016.
//  Copyright © 2016 Gabriel Massana. All rights reserved.
//

import UIKit

internal enum SectionType {
    
    case post
    case user
    case userAddress
    case userCompany
    
    func sectionTitle() -> String {
        
        switch self {
            
        case .post:
            
            return NSLocalizedString("Post", comment: "post_title")
            
        case .user:
            
            return NSLocalizedString("User", comment: "user_title")
            
        case .userAddress:
            
            return NSLocalizedString("User Address", comment: "user_address_title")
            
        case .userCompany:
            
            return NSLocalizedString("User Company", comment: "user_company_title")
        }
    }
}

internal enum RowType {
    
    case postTitle
    case postBody
    case postTotalComments
    case user
    case userAddress
    case userCompany
    
    func rowCell(post: Post) -> UITableViewCell {
        
        switch self {
            
        case .postTitle:
            
            let cell = PostTitleCell(post: post)
            cell.layoutByApplyingConstraints()
            
            return cell
            
        case .postBody:
            
            let cell = PostBodyCell(post: post)
            cell.layoutByApplyingConstraints()
            
            return cell
            
        case .postTotalComments:
            
            let cell = PostTotalCommentsCell(post: post)
            cell.layoutByApplyingConstraints()
            
            return cell
            
        case .user:
            
            let cell = UserCell(post: post)
            cell.layoutByApplyingConstraints()
            
            return cell
            
        case .userAddress:
            
            let cell = UserAddressCell(post: post)
            cell.layoutByApplyingConstraints()
            
            return cell
            
        case .userCompany:
            
            let cell = UserCompanyCell(post: post)
            cell.layoutByApplyingConstraints()
            
            return cell
        }
    }
}

internal struct Section {
    
    var section: SectionType
    var rows: [RowType]
}

class DetailsAdapter: NSObject {

    //MARK: - Accessors
    
    var post: Post
    
    var heightAtIndexPath = [IndexPath : NSNumber]()

    var sections: [Section] = {
       
        var sections = [
        
            Section(section: .post, rows: [.postTitle, .postBody, .postTotalComments]),
            Section(section: .user, rows: [.user]),
            Section(section: .userAddress, rows: [.userAddress]),
            Section(section: .userCompany, rows: [.userCompany])
        ]
        
        return sections
    }()
    
    /// Table view to display data.
    var tableView: UITableView? {
        
        willSet (newValue) {
            
            if newValue != tableView {
                
                self.tableView = newValue
                
                tableView?.dataSource = self
                tableView?.delegate = self
                tableView?.backgroundColor = UIColor.white
                tableView?.separatorStyle = .none
                
                tableView?.rowHeight = UITableViewAutomaticDimension
                tableView?.estimatedRowHeight = UITableViewAutomaticDimension
            }
        }
    }
    
    //MARK: - Init
    
    init(post: Post) {
        
        self.post = post
        
        super.init()
    }
}

extension DetailsAdapter: UITableViewDataSource {
    
    //MARK: - UITableViewDataSource
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return sections[section].rows.count
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        
        return sections.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let rowType = sections[indexPath.section].rows[indexPath.row]
        
        return rowType.rowCell(post: post)
    }
    
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        
        let sectionType = sections[section].section
        
        return sectionType.sectionTitle()
    }
}

extension DetailsAdapter: UITableViewDelegate {
 
    //MARK: - UITableViewDelegate
    
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        
        let height = heightAtIndexPath[indexPath]
        
        if let height = height {
            
            return CGFloat(height)
        }
        else {
            
            return UITableViewAutomaticDimension
        }
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        
        let height: NSNumber = NSNumber(value: Float(cell.frame.height))
        heightAtIndexPath[indexPath] = height
    }
    
    func tableView(_ tableView: UITableView, didEndDisplaying cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        
        let height: NSNumber = NSNumber(value: Float(cell.frame.height))
        heightAtIndexPath[indexPath] = height
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        
        return 35.0
    }
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        
        return 0.01
    }
}
