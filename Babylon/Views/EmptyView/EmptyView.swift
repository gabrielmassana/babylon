//
//  EmptyView.swift
//  Babylon
//
//  Created by Gabriel Massana on 30/10/2016.
//  Copyright © 2016 Gabriel Massana. All rights reserved.
//

import UIKit

class EmptyView: UIView {

    //MARK: - Accessors
    
    lazy var titleLabel: UILabel = {
        
        var titleLabel = UILabel.newAutoLayout()
        
        titleLabel.font = UIFont.tradeGothicLTWithSize(18.0)
        titleLabel.textAlignment = .center
        titleLabel.textColor = UIColor.black
        titleLabel.numberOfLines = 0
        titleLabel.text = NSLocalizedString("Empty Table", comment: "empty_title")
        
        return titleLabel
    }()
    
    //MARK: - Init
    
    override init(frame: CGRect) {
        
        super.init(frame: frame)
        
        backgroundColor = UIColor.alto()
        addSubview(titleLabel)
    }
    
    required init?(coder aDecoder: NSCoder) {
        
        fatalError("init(coder:) has not been implemented")
    }
    
    //MARK: - Constraints
    
    override func updateConstraints() {
        
        titleLabel.autoPinEdge(toSuperviewEdge: .left)
        titleLabel.autoPinEdge(toSuperviewEdge: .right)
        titleLabel.autoPinEdge(toSuperviewEdge: .top,
                               withInset: 100.0 * DeviceManager.sharedInstance.resizeFactor)
        
        /*-------------------*/
        
        super.updateConstraints()
    }
}
