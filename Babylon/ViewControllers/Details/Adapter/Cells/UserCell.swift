//
//  UserCell.swift
//  Babylon
//
//  Created by Gabriel Massana on 28/10/2016.
//  Copyright © 2016 Gabriel Massana. All rights reserved.
//

import UIKit

class UserCell: UITableViewCell, TableViewCell {
    
    //MARK: - Accessors
    
    var post: Post
    
    lazy var userLabel: UILabel = {
        
        var userLabel = UILabel.newAutoLayout()
        
        userLabel.font = UIFont.tradeGothicLTWithSize(16.0)
        userLabel.textAlignment = .left
        userLabel.textColor = UIColor.black
        userLabel.numberOfLines = 0
        
        return userLabel
    }()
    
    //MARK: - Init
    
    init(post: Post) {
        
        self.post = post
        
        super.init(style: .default,
                   reuseIdentifier: nil)
        
        selectionStyle = .none
        
        contentView.addSubview(userLabel)
        
        configureWithPost(post)
    }
    
    required init?(coder aDecoder: NSCoder) {
        
        fatalError("init(coder:) has not been implemented")
    }
    
    //MARK: - Constraints
    
    override func updateConstraints() {
        
        userLabel.autoPinEdge(toSuperviewEdge: .left,
                              withInset: 15.0 * DeviceManager.sharedInstance.resizeFactor)
        
        userLabel.autoPinEdge(toSuperviewEdge: .right,
                              withInset: 15.0 * DeviceManager.sharedInstance.resizeFactor)
        
        userLabel.autoPinEdge(toSuperviewEdge: .top,
                              withInset: 10.0 * DeviceManager.sharedInstance.resizeFactor)
        
        NSLayoutConstraint.autoSetPriority(UILayoutPriorityDefaultHigh) {
            
            userLabel.autoPinEdge(toSuperviewEdge: .bottom,
                                  withInset: 10.0 * DeviceManager.sharedInstance.resizeFactor)
        }
        
        /*-------------------*/
        
        super.updateConstraints()
    }
    
    //MARK: - ConfigureCell
    
    /**
     Configure the self cell based on the post data.
     
     - Parameter post: The post with the data related to the cell.
     */
    private func configureWithPost(_ post: Post) {
        
        self.post = post
        
        guard let user = post.user,
            let name = user.name,
            let username = user.username,
            let email = user.email,
            let phone = user.phone else {
            
                return
        }
        
        let nameTitle = "\(NSLocalizedString("Name: ", comment: "name_title"))\(name)\n"
        let usernameTitle = "\(NSLocalizedString("Username: ", comment: "username_title"))\(username)\n"
        let emailTitle = "\(NSLocalizedString("Email: ", comment: "email_title"))\(email)\n"
        let phoneTitle = "\(NSLocalizedString("Phone: ", comment: "phone_title"))\(phone)"
        
        userLabel.text = nameTitle + usernameTitle + emailTitle + phoneTitle
    }
}
