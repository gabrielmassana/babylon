//
//  Comment+CoreDataClass.swift
//  
//
//  Created by Gabriel Massana on 26/10/2016.
//
//

import Foundation

import CoreData
import CoreDataFullStack

@objc(Comment)
public class Comment: NSManagedObject {

    class func fetchComment(_ commentID: String, managedObjectContext: NSManagedObjectContext) -> Comment? {
        
        let predicate = NSPredicate(format: "commentID == %@", commentID)
        
        let comment = CDFRetrievalService.retrieveFirstEntry(forEntityClass: Comment.self,
                                                             predicate: predicate,
                                                             managedObjectContext: managedObjectContext) as? Comment
        
        return comment
    }
}
