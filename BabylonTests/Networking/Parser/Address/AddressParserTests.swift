//
//  AddressParserTests.swift
//  Babylon
//
//  Created by Gabriel Massana on 30/10/2016.
//  Copyright © 2016 Gabriel Massana. All rights reserved.
//

import XCTest

import CoreDataFullStack

@testable import Babylon

class AddressParserTests: XCTestCase {
    
    //MARK: - Accessors
    
    var parser: AddressParser?

    var addressJSON: [String : AnyObject]?
    
    var userID: String?
    var street: String?
    var suite: String?
    var city: String?
    var zipcode: String?
    var lat: String?
    var lng: String?
    var geo: [String : AnyObject]?
    
    // MARK: - TestSuiteLifecycle
    
    override func setUp() {
        
        super.setUp()
        
        parser = AddressParser.parser(CDFCoreDataManager.sharedInstance().managedObjectContext)
        
        userID = "1"

        street = "Kulas Light"
        suite = "Apt. 556"
        city = "Gwenborough"
        zipcode = "92998-3874"
        
        lat = "-37.3159"
        lng = "81.1496"
        geo = [
            "lat" : lat as AnyObject,
            "lng" : lng as AnyObject
        ]
        
        addressJSON = [
            "street" : "Kulas Light" as AnyObject,
            "suite" : "Apt. 556" as AnyObject,
            "city" : "Gwenborough" as AnyObject,
            "zipcode" : "92998-3874" as AnyObject,
            "geo" : geo as AnyObject
        ]
    }
    
    override func tearDown() {
        
        CDFCoreDataManager.sharedInstance().reset()
        
        parser = nil
        
        street = nil
        suite = nil
        city = nil
        zipcode = nil
        geo = nil
        
        addressJSON = nil
        
        super.tearDown()
    }
    
    // MARK: - AddressParser
    
    func test_addressParser_newAddressObjectReturned() {
        
        let address = parser?.parseAddress(addressJSON!,
                                           userID: userID!)
        
        XCTAssertNotNil(address, "A valid Address object wasn't created")
    }
    
    func test_parseAddress_street() {
        
        let address = parser?.parseAddress(addressJSON!,
                                           userID: userID!)
        
        XCTAssertEqual(address!.street, street!, String(format:"Parsed object is wrong: %@", address!))
    }
    
    func test_parseAddress_suite() {
        
        let address = parser?.parseAddress(addressJSON!,
                                           userID: userID!)
        
        XCTAssertEqual(address!.suite, suite!, String(format:"Parsed object is wrong: %@", address!))
    }
    
    func test_parseAddress_city() {
        
        let address = parser?.parseAddress(addressJSON!,
                                           userID: userID!)
        
        XCTAssertEqual(address!.city, city!, String(format:"Parsed object is wrong: %@", address!))
    }
    
    func test_parseAddress_zipcode() {
        
        let address = parser?.parseAddress(addressJSON!,
                                           userID: userID!)
        
        XCTAssertEqual(address!.zipcode, zipcode!, String(format:"Parsed object is wrong: %@", address!))
    }
    
    func test_parseAddress_latitude() {
        
        let address = parser?.parseAddress(addressJSON!,
                                           userID: userID!)
        
        XCTAssertEqual(address!.latitude!.stringValue, lat!, String(format:"Parsed object is wrong: %@", address!))
    }
    
    func test_parseAddress_longitude() {
        
        let address = parser?.parseAddress(addressJSON!,
                                           userID: userID!)
        
        XCTAssertEqual(address!.longitude!.stringValue, lng!, String(format:"Parsed object is wrong: %@", address!))
    }
}
