//
//  PostTotalCommentsCell.swift
//  Babylon
//
//  Created by Gabriel Massana on 29/10/2016.
//  Copyright © 2016 Gabriel Massana. All rights reserved.
//

import UIKit

class PostTotalCommentsCell: UITableViewCell, TableViewCell {
    
    //MARK: - Accessors
    
    var post: Post
    
    lazy var postTotalCommentsLabel: UILabel = {
        
        var postTotalCommentsLabel = UILabel.newAutoLayout()
        
        postTotalCommentsLabel.font = UIFont.tradeGothicLTWithSize(16.0)
        postTotalCommentsLabel.textAlignment = .left
        postTotalCommentsLabel.textColor = UIColor.black
        postTotalCommentsLabel.numberOfLines = 0
        
        return postTotalCommentsLabel
    }()
    
    //MARK: - Init
    
    init(post: Post) {
        
        self.post = post
        
        super.init(style: .default,
                   reuseIdentifier: nil)
        
        selectionStyle = .none
        
        contentView.addSubview(postTotalCommentsLabel)
        
        configureWithPost(post)
    }
    
    required init?(coder aDecoder: NSCoder) {
        
        fatalError("init(coder:) has not been implemented")
    }
    
    //MARK: - Constraints
    
    override func updateConstraints() {
        
        postTotalCommentsLabel.autoPinEdge(toSuperviewEdge: .left,
                                           withInset: 15.0 * DeviceManager.sharedInstance.resizeFactor)
        
        postTotalCommentsLabel.autoPinEdge(toSuperviewEdge: .right,
                                           withInset: 15.0 * DeviceManager.sharedInstance.resizeFactor)
        
        postTotalCommentsLabel.autoPinEdge(toSuperviewEdge: .top,
                                           withInset: 10.0 * DeviceManager.sharedInstance.resizeFactor)
        
        NSLayoutConstraint.autoSetPriority(UILayoutPriorityDefaultHigh) {
            
            postTotalCommentsLabel.autoPinEdge(toSuperviewEdge: .bottom,
                                               withInset: 10.0 * DeviceManager.sharedInstance.resizeFactor)
        }
        
        /*-------------------*/
        
        super.updateConstraints()
    }
    
    //MARK: - ConfigureCell
    
    /**
     Configure the self cell based on the post data.
     
     - Parameter post: The post with the data related to the cell.
     */
    private func configureWithPost(_ post: Post) {
        
        self.post = post

        var text = NSLocalizedString("Comments: ", comment: "total_comments_title")
        
        if let comments = post.comments?.count {
            
            text = "\(text)\(comments)"
        }
        
        postTotalCommentsLabel.text = text
    }
}
