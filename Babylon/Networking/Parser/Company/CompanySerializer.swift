//
//  CompanySerializer.swift
//  Babylon
//
//  Created by Gabriel Massana on 27/10/2016.
//  Copyright © 2016 Gabriel Massana. All rights reserved.
//

import Foundation

struct CompanySerializer {
    
    var bs: String?
    var catchPhrase: String?
    var name: String?
    
    init?(serverResponse: [String : AnyObject]) {
        
        if let bs = serverResponse["bs"] as? String {
            
            self.bs = bs
        }
        
        if let catchPhrase = serverResponse["catchPhrase"] as? String {
            
            self.catchPhrase = catchPhrase
        }
        
        if let name = serverResponse["name"] as? String {
            
            self.name = name
        }
    }
}
