//
//  Operation.swift
//  Babylon
//
//  Created by Gabriel Massana on 26/10/2016.
//  Copyright © 2016 Gabriel Massana. All rights reserved.
//

import UIKit
import Foundation

/// Success clousure.
typealias OperationOnSuccess = @convention(block) (_ result: AnyObject?) -> Void

/// Failure clousure.
typealias OperationOnFailure = @convention(block) (_ error: NSError?) -> Void

/// Completion clousure.
typealias OperationOnCompletion = @convention(block) (_ result: AnyObject?) -> Void

/// Subclass of NSOperation with callbacks and auto coalesce.
class Operation: Foundation.Operation {
    
    //MARK: - Accessors
    
    /// Unique identifier used for coalescing, operations with the same identifier will be coalesced in the operation coordinator.
    var identifier: String?
    
    /// This is the identifier of the operation queue you would like the operation to be run on.
    var operationQueueIdentifier: String?
    
    /// Callback called when the operation pletes succesfully.
    var onSuccess: OperationOnSuccess?
    
    /// Callback called when the operation fails.
    var onFailure: OperationOnFailure?
    
    /// Callback called when the operation completes. The completion block is used instead of the success/failure blocks not alongside.
    var onCompletion: OperationOnCompletion?
    
    /// The result of the operation.
    var result: AnyObject?
    
    /// The error of the operation.
    var error: NSError?
    
    /// Current Operation Queue.
    var callbackQueue: OperationQueue?
    
    fileprivate var _ready: Bool = true
    fileprivate var _executing: Bool = false
    fileprivate var _finished: Bool = false
    fileprivate var _identifier: String?
    
    //MARK: - Init
    
    override init() {
        
        super.init()
        
        self.isReady = true
        callbackQueue = OperationQueue.current
    }
    
    //MARK: - Name
    
    override var name: String? {
        
        get {
            
            return identifier
        }
        set {
            
        }
    }
    
    //MARK: - State
    
    override var isReady: Bool {
        
        get {
            
            return _ready
        }
        set {
            
            if _ready != newValue {
                
                willChangeValue(forKey: "isReady")
                _ready = newValue
                didChangeValue(forKey: "isReady")
            }
        }
    }
    
    override var isExecuting: Bool {
        
        get {
            
            return _executing
        }
        set {
            
            if _executing != newValue {
                
                willChangeValue(forKey: "isExecuting")
                _executing = newValue
                didChangeValue(forKey: "isExecuting")
            }
        }
    }
    
    override var isFinished: Bool {
        
        get {
            
            return _finished
        }
        set {
            
            if _finished != newValue {
                
                willChangeValue(forKey: "isFinished")
                _finished = newValue
                didChangeValue(forKey: "isFinished")
            }
        }
    }
    
    override var isAsynchronous: Bool {
        
        get {
            
            return true
        }
        set {
            
        }
    }
    
    //MARK: - Control
    
    override func start() {
        
        if !isExecuting {
            
            isReady = false
            isExecuting = true
            isFinished = false
            
            if let name = self.name {
                
                print("\(name) - Operation will start.")
            }
            else {
                
                print("\(self.name) - Operation will start.")
            }
        }
    }
    
    /**
     Finishes the execution of the operation.
     
     - Note: This shouldn't be called externally as this is used internally by subclasses.
     To cancel an operation use cancel instead.
     */
    func finish() {
        
        if isExecuting {
            
            if let name = self.name {
                
                print("\(name) - Operation did finish.")
            }
            else {
                
                print("\(self.name) - Operation did finish.")
            }
            
            isExecuting = false
            isFinished = true
        }
    }
    
    //MARK: - Callbacks
    
    /**
     Finishes the execution of the operation and calls the onSuccess callback.
     
     - Note: This shouldn't be called externally as this is used internally by subclasses.
     To cancel an operation use cancel instead.
     */
    func didSucceedWithResult(_ result: AnyObject?) {
        
        self.result = result
        
        finish()
        
        if let onSuccess = onSuccess {
            
            callbackQueue?.addOperation({
                
                onSuccess(result)
            })
        }
    }
    
    /**
     Finishes the execution of the operation and calls the onFailure callback.
     
     - Note: This shouldn't be called externally as this is used internally by subclasses.
     To cancel an operation use cancel instead.
     */
    func didFailWithError(_ error: NSError?) {
        
        self.error = error
        
        finish()
        
        if let onFailure = onFailure {
            
            callbackQueue?.addOperation({
                
                onFailure(error)
            })
        }
    }
    
    /**
     Finishes the execution of the operation and calls the onCompletion callback.
     
     - Note: This shouldn't be called externally as this is used internally by subclasses.
     To cancel an operation use cancel instead.
     */
    func didCompleteWithResult(_ result: AnyObject?) {
        
        self.result = result
        
        finish()
        
        if let onCompletion = onCompletion {
            
            callbackQueue?.addOperation({
                
                onCompletion(result)
            })
        }
    }
    
    //MARK: - Coalescing
    
    /**
     This method figures out if we can coalesce with another operation.
     
     - Parameter operation: Operation to determaine if we can coalesce with.
     - Returns: YES if we can coaslesce with it. NO otherwise.
     */
    func canCoalesceWithOperation(_ operation: Operation) -> Bool {
        
        return identifier == operation.identifier
    }
    
    /**
     This method coalesces another operation with this one.
     
     - Parameter operation: Operation to coalesce with.
     */
    func coalesceWithOperation(_ operation: Operation) {
        
        // Success coalescing
        let mySuccessBlock = onSuccess
        let theirSuccessBlock = operation.onSuccess
        
        if mySuccessBlock != nil ||
            theirSuccessBlock != nil {
            
            onSuccess = { (result: AnyObject?) in
                
                mySuccessBlock?(result)
                theirSuccessBlock?(result)
            }
        }
        
        // Failure coalescing
        let myFailureBlock = onFailure
        let theirFailureBlock = operation.onFailure
        
        if myFailureBlock != nil ||
            theirFailureBlock != nil {
            
            onFailure = { (error: NSError?) in
                
                myFailureBlock?(error)
                theirFailureBlock?(error)
            }
        }
        
        // Completion coalescing
        let myCompletionBlock = onCompletion
        let theirCompletionBlock = operation.onCompletion
        
        if myCompletionBlock != nil ||
            theirCompletionBlock != nil {
            
            onCompletion = { (result: AnyObject?) in
                
                myCompletionBlock?(result)
                theirCompletionBlock?(result)
            }
        }
    }
}
