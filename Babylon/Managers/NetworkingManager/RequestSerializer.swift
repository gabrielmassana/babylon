//
//  RequestSerializer.swift
//  Babylon
//
//  Created by Gabriel Massana on 26/10/2016.
//  Copyright © 2016 Gabriel Massana. All rights reserved.
//

import Alamofire

extension Alamofire.DataRequest {
    
    /// Handle the API response and parse the errors on it.
    @discardableResult
    func responseJSONBabylon(
        queue: DispatchQueue? = nil,
        options: JSONSerialization.ReadingOptions = .allowFragments,
        completionHandler: @escaping (DataResponse<Any>, NSError) -> Void)
        -> Self {
            
            return self.responseJSON { (response) in
                
                let error = ErrorSerializer.serializeError(response.result.error as NSError?,
                                                           data: response.data)
                
                completionHandler(response, error)
            }
    }
}
