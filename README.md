[![BuddyBuild](https://dashboard.buddybuild.com/api/statusImage?appID=581302e2ce97730100040b0f&branch=master&build=latest)](https://dashboard.buddybuild.com/apps/581302e2ce97730100040b0f/build/latest)

# Babylon

- [http://jsonplaceholder.typicode.com/posts](http://jsonplaceholder.typicode.com/posts)
- [http://jsonplaceholder.typicode.com/users](http://jsonplaceholder.typicode.com/users)
- [http://jsonplaceholder.typicode.com/comments](http://jsonplaceholder.typicode.com/comments)

### Technical Decisions

 - Xcode 8.0
 - Language: Swift 3.X
 - iOS >=9.2
 - iPhone >=5
 - Core Data to persist data
 - NSURLSession through Alamofire
 - NSOperaionQueue for background operations
 
### Unit Test

I added some unit tests to the project. I know they are a really small part of the tests that can be added to this project, but they are only trying to be a small example of how powerful they are. [Test Coverage over 33%](https://dashboard.buddybuild.com/apps/581302e2ce97730100040b0f/build/latest). 

### Continuous Integration

The project is integrated with [BuddyBuild](https://dashboard.buddybuild.com/apps/581302e2ce97730100040b0f/build/latest) as Continuous Integration to automate the build and test of the project.

### My own Pods

I used one of [my own pods in Cocoapods](https://cocoapods.org/owners/10374).   
   
- **[ColorWithHex](https://cocoapods.org/pods/ColorWithHex)**. Swift Extension to convert hexadecimal values into `UIColor` Objects.
- **[CoreDataFullStack](https://cocoapods.org/pods/CoreDataFullStack)**. A suite of helper classes to help to remove some of the boilerplate that surrounds using Core Data 
- **[TableViewDataStatus](https://cocoapods.org/pods/TableViewDataStatus)**. Table View to control Data Status: empty and Loading Table Views

### Third party Pods

- **[PureLayout](https://cocoapods.org/pods/PureLayout)**. An easy and powerfull pod that helps a lot using auto-layout.
- **[Alamofire](https://cocoapods.org/pods/Alamofire)**. An HTTP networking library written in Swift.
- **[BuddyBuildSDK](https://cocoapods.org/pods/BuddyBuildSDK)**. Buddy Build SDK to gather feedback.

# The app meets all the requirements

- Use Swift 3.0.
- The information (posts and post details) should be available offline. It's assumed that if it's the first time you are accessing the app, and you are offline, you shouldn't see anything.
- The code should be production grade.
- It should compile and run.

## About Tips/Advices

- The app is downloading all the data on first launch of the app and storing it into Core Data. Even though the data is not changing, every time the app opens teh app is calling refresh() function to update the data.

- Some Unit tests added. Test Coverage over 33%.

- Added ErrorSerializer class to handle Errors from the API. Every time the app calls the API, the ErrorSerializer can serialize an error depending on the response.The ErrorSerializer is just a prove of concept right now.

- Knowing nothing about how the app can grow in the future, it is obvious that different classes have different responsabilities.

- I love code that follows a Coding Style. However, I do not mind adapting myself to a new coding style. I'm following an evolved Coding Style similar to RayWenderlich one.

- Whole project structured in folders. Folders in project correspond to folders in the Finder.