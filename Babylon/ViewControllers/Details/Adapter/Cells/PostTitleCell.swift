//
//  PostTitleCell.swift
//  Babylon
//
//  Created by Gabriel Massana on 29/10/2016.
//  Copyright © 2016 Gabriel Massana. All rights reserved.
//

import UIKit

class PostTitleCell: UITableViewCell, TableViewCell {

    //MARK: - Accessors
    
    var post: Post
    
    lazy var postTitleLabel: UILabel = {
        
        var postTitleLabel = UILabel.newAutoLayout()
        
        postTitleLabel.font = UIFont.tradeGothicLTWithSize(18.0)
        postTitleLabel.textAlignment = .left
        postTitleLabel.textColor = UIColor.black
        postTitleLabel.numberOfLines = 0
        
        return postTitleLabel
    }()
    
    //MARK: - Init
    
    init(post: Post) {
        
        self.post = post
        
        super.init(style: .default,
                   reuseIdentifier: nil)
        
        selectionStyle = .none
        
        contentView.addSubview(postTitleLabel)
        
        configureWithPost(post)
    }
    
    required init?(coder aDecoder: NSCoder) {
        
        fatalError("init(coder:) has not been implemented")
    }
    
    //MARK: - Constraints
    
    override func updateConstraints() {
        
        postTitleLabel.autoPinEdge(toSuperviewEdge: .left,
                                   withInset: 15.0 * DeviceManager.sharedInstance.resizeFactor)
        
        postTitleLabel.autoPinEdge(toSuperviewEdge: .right,
                                   withInset: 15.0 * DeviceManager.sharedInstance.resizeFactor)
        
        postTitleLabel.autoPinEdge(toSuperviewEdge: .top,
                                   withInset: 10.0 * DeviceManager.sharedInstance.resizeFactor)
        
        NSLayoutConstraint.autoSetPriority(UILayoutPriorityDefaultHigh) {
            
            postTitleLabel.autoPinEdge(toSuperviewEdge: .bottom,
                                       withInset: 10.0 * DeviceManager.sharedInstance.resizeFactor)
        }
        
        /*-------------------*/
        
        super.updateConstraints()
    }
    
    //MARK: - ConfigureCell
    
    /**
     Configure the self cell based on the post data.
     
     - Parameter post: The post with the data related to the cell.
     */
    private func configureWithPost(_ post: Post) {
        
        self.post = post
        
        postTitleLabel.text = post.title
    }
}
